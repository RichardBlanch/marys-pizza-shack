//
//  CustomSegment.swift
//  Mary's Pizza Shack
//
//  Created by Rich Blanchard on 8/10/15.
//  Copyright (c) 2015 Rich. All rights reserved.
//

import UIKit
var segmentArrow:UIImageView!
let kStyledSegmentedControlHeight = 41

class CustomSegment: UISegmentedControl {
    override  init(frame: CGRect) {
        super.init(frame: frame)
        
        backgroundColor = UIColor.whiteColor()
        let font = UIFont(name: "Cabin-Regular", size: 12.0)
        let fontColor = UIColor(red: 37, green: 77, blue: 37, alpha: 1.0)
        self.setTitleTextAttributes([NSForegroundColorAttributeName : fontColor], forState: .Normal)
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        selectedSegmentIndex = 0
        self.backgroundColor = UIColor.whiteColor()
        let font = UIFont(name: "Cabin-Regular", size: 30)
        let fontColor = UIColor(red: 37/255, green: 77/255, blue: 37/255, alpha: 1.0)
       
        
        
      
        
        if let font = UIFont(name: "Cabin-Bold", size: 12) {
            let colour = UIColor.marysRed()
            let attributes: [NSString : AnyObject] = [NSFontAttributeName: font, NSForegroundColorAttributeName: colour]
           for _ in self.subviews
           {
            self.setTitleTextAttributes(attributes, forState: .Selected)
            self.setTitleTextAttributes(attributes, forState: .Normal)
            self.setTitleTextAttributes(attributes, forState: .Highlighted)
            self.tintColor = UIColor(red: 182/255, green: 38/255, blue: 38/255, alpha: 1.0)
            }
        }
       
        
        segmentArrow = UIImageView(image: UIImage(named: "segment_arrow"))
        self.addTarget(self, action: ("selectedSegmentChanged"), forControlEvents: .ValueChanged)

    }
    func selectedSegmentChanged()
    {
        backgroundColor = UIColor.whiteColor() 
        segmentArrow.removeFromSuperview()
        let segmentView = self.subviews[selectedSegmentIndex] 
        let fontColor = UIColor(red: 37/255, green: 77/255, blue: 37/255, alpha: 1.0)
        self.setTitleTextAttributes([NSForegroundColorAttributeName : fontColor], forState: .Normal)
        if let font = UIFont(name: "Cabin-Bold", size: 18) {
            let fontTwo = UIFont(name: "Cabin-Bold", size: 12)!
            let colour = UIColor(red: 37/255, green: 77/255, blue: 37/255, alpha: 1.0)
            let attributes: [NSString : AnyObject] = [NSFontAttributeName: font, NSForegroundColorAttributeName: colour]
            let attributesNotSelected: [NSString : AnyObject] = [NSFontAttributeName: fontTwo, NSForegroundColorAttributeName: colour]
            
            self.setTitleTextAttributes(attributes, forState: .Selected)
            self.setTitleTextAttributes(attributesNotSelected, forState: .Normal)
            self.setTitleTextAttributes(attributesNotSelected, forState: .Highlighted)
        segmentView.tintColor = UIColor(red: 182/255, green: 38/255, blue: 38/255, alpha: 1.0)
        segmentView.addSubview(segmentArrow)
        segmentArrow.center = CGPointMake(segmentView.frame.size.width / 2 , 41.0 + segmentArrow.frame.size.height / 2);
        
    }
    }
    
    
    
    
}
