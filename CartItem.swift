//
//  CartItem.swift
//  Mary's Pizza Shack
//
//  Created by Rich Blanchard on 8/11/15.
//  Copyright (c) 2015 Rich. All rights reserved.
//

import Foundation
class CartItem : NSObject {

    init(price: String, item: String, extras: [String]?, sauces: String?, size: String?) {
        self.price = price
        self.item = item
        self.extras = extras
        self.sauces = sauces
        self.size = size
        super.init()
    }
  
    
    var price :String!
    let item:String!
    var extras: [String]?
    let sauces: String?
    let size: String?
    
}

