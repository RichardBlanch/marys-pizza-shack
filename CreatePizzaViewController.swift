//
//  CreatePizzaViewController.swift
//  Mary's Pizza Shack
//
//  Created by Rich Blanchard on 8/16/15.
//  Copyright (c) 2015 Rich. All rights reserved.
//

import UIKit

class CreatePizzaViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, addTopping {
    var traditionalToppings : Set<PizzaWithTopping>!
    var specialtyToppings : Set<PizzaWithTopping>!
    
    @IBOutlet weak var backgroundView: UIView!
    
    var addedTradtionalToppings : Set<PizzaWithTopping>!
    var addedSpecialtyToppings : Set<PizzaWithTopping>!
    var selectedSize : String!
    
    
    
    
    @IBOutlet weak var tableView: UITableView!
    
    
    
    @IBOutlet weak var sizeSegment: UISegmentedControl!
    
    
    @IBOutlet weak var priceLabel: UILabel!
    
    
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        tabBarController?.tabBar.hidden = false
        navigationController?.navigationBar.hidden = true
        backgroundView.backgroundColor = UIColor.yellowGifBackground()
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        
        
        //get all of the availableToppings
        traditionalToppings =  Toppings.getTradtionalToppings()
        specialtyToppings = Toppings.getSpecialtyToppings()
        
        //create empty arrays for toppings user had added
        addedTradtionalToppings = []
        addedSpecialtyToppings = []
        
        
        sizeSegment.selectedSegmentIndex = 0
        tableView.separatorColor = UIColor.marysRed()
        
        
    }
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        
        return 2
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            
            
            return traditionalToppings.count
        }
        else {
            return specialtyToppings.count
            
            
        }
    }
    func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if section == 0 {
            return "Traditional Toppings"
        }
        else {
            return "Specialty Toppings"
        }
    }
    
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath) as! CreatePizzaTableViewCell
        
        cell.delegate = self
        if indexPath.section == 0 {
            let pizzaWithTopping:PizzaWithTopping! = traditionalToppings[traditionalToppings.startIndex.advancedBy(indexPath.row)]
            cell.topping = pizzaWithTopping
            cell.toppingLabel.text = pizzaWithTopping.topping
            cell.addToppingSwitch.on = pizzaWithTopping.on
            
        }
        if indexPath.section == 1 {
            let pizzaWithTopping:PizzaWithTopping! = specialtyToppings[specialtyToppings.startIndex.advancedBy(indexPath.row)]
            cell.topping = pizzaWithTopping
            cell.toppingLabel.text = pizzaWithTopping.topping
            cell.addToppingSwitch.on = pizzaWithTopping.on
            
        }
        
        
        return cell
    }
    
    
    
    
    
    
    func addTopping(pizzaTopping: PizzaWithTopping) {
        if(specialtyToppings.contains(pizzaTopping)) //if the topping the user has added is a "specialty", add it to specialty set
        {
            addedSpecialtyToppings.insert(pizzaTopping)
        }
        else if(traditionalToppings.contains(pizzaTopping)) //else put it in traditional set
        {
            addedTradtionalToppings.insert(pizzaTopping)
            
            
        }
        changeSizePrice(sizeSegment) //change our price
        
    }
    func removeTopping(pizzaTopping: PizzaWithTopping) //same thing but removing a topping
    {
        if(specialtyToppings.contains(pizzaTopping)) {
            addedSpecialtyToppings.remove(pizzaTopping)
        }
        else if (traditionalToppings.contains(pizzaTopping))
        {
            addedTradtionalToppings.remove(pizzaTopping)
        }
        changeSizePrice(sizeSegment)
    }
    
    
    
    
    
    
    func changePriceLabel(size:UISegmentedControl)->Double {
        var price = 0.0
        switch(size.selectedSegmentIndex) {
        case 0:
            price = 7.95
        case 1:
            price = 11.95
        case 2:
            price = 15.25
        case 3:
            price = 18.95
        default:
            print("This should never print", terminator: "")
        }
        return price
        
    }
    
    @IBAction func changeSizePrice(sender: AnyObject) {
        let price = changePriceLabel(sizeSegment)
        var totalPrice = price
        let traditionalInflation = getInflation(sizeSegment).traditionalInflation
        let specialtyInflation = getInflation(sizeSegment).specialtyInflation
        
        for topping in addedTradtionalToppings {
            totalPrice += traditionalInflation
        }
        for topping in addedSpecialtyToppings {
            totalPrice += specialtyInflation
        }
        let formattedPrice = CurrencyFormatter.sharedInstance.stringFromNumber(NSNumber(double: totalPrice))
        priceLabel.text = formattedPrice
        
        
    }
    
    
    func getInflation(segment:UISegmentedControl)->(traditionalInflation:Double, specialtyInflation:Double) {
        switch(segment.selectedSegmentIndex) { //going to add different amounts for size the user has selected
        case 0:
            return(traditionalInflation:1.25, specialtyInflation:1.95)
        case 1:
            return(traditionalInflation:1.50, specialtyInflation:2.25)
        case 2:
            return(traditionalInflation:1.95, specialtyInflation:2.50)
        case 3:
            return(traditionalInflation:2.30, specialtyInflation:3.00)
        default:
            return(traditionalInflation:1.25, specialtyInflation:1.95)
            
        }
    }
    override func  prefersStatusBarHidden() -> Bool {
        return true
    }
    
    @IBAction func addToCart(sender: UIButton) {
        var combinedArray : [String]!
        combinedArray = []
        if addedSpecialtyToppings.count > 0
        {
            for topping in addedSpecialtyToppings {
                combinedArray.append(topping.topping)
            }
            
        }
        if(addedTradtionalToppings.count > 0)
        {
            for topping in addedTradtionalToppings {
                
                combinedArray.append(topping.topping)
            }
        }
        let cartItem = CartItem(price: priceLabel.text!, item: "Create-Your-Own-Pizza", extras: combinedArray, sauces: nil, size: sizeSegment.titleForSegmentAtIndex(sizeSegment.selectedSegmentIndex))
        Cart.sharedCart.addItem(cartItem)
        var mutableString = "Cheese"
        if combinedArray.count > 0
        {
            mutableString = combinedArray[0]
            for var i = 1; i < combinedArray.count; i++ {
                mutableString += ", "
                mutableString += combinedArray[i]
            }
        }
        
        
        let banner = Banner(title: "Added Item:", subtitle: "\(cartItem.size!) Pizza with \(mutableString)", image: UIImage(named: "Buy-32"), backgroundColor: UIColor.marysGreen()) { () -> () in
            UIView.transitionWithView(self.view, duration: 1.0, options: UIViewAnimationOptions.TransitionFlipFromTop, animations: { () -> Void in
                self.view.alpha = 0.0
                
                }, completion: { (sucess) -> Void in
                    
                    self.performSegueWithIdentifier("cartSegue", sender: self)
            })
        }
        banner.show(self.view, duration: 4.0) //bring down the banenr
        banner.textColor = UIColor.whiteColor()
        priceLabel.text = "$7.95" //reset our price
        sizeSegment.selectedSegmentIndex = 0 //reset segment
        //        for detail detail tableViewController make extras appear in diffrent cells
        addedSpecialtyToppings = [] //reset arrays
        addedTradtionalToppings = []
        for pizzaWithTopping in traditionalToppings {
            pizzaWithTopping.on = false //turn all toppings off
            tableView.reloadData() //reload data
            
        }
        for pizzaWithTopping in specialtyToppings {
            pizzaWithTopping.on = false
            tableView.reloadData()
        }
        
    }
    
    
    
}






