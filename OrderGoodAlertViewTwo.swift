//
//  OrderGoodAlertViewTwo.swift
//  Mary's Pizza Shack
//
//  Created by Rich Blanchard on 8/17/15.
//  Copyright (c) 2015 Rich. All rights reserved.
//

import UIKit

class OrderGoodAlertViewTwo: UIView {
    let dynamicAnimator: UIDynamicAnimator!
    let gravityBehavior : UIGravityBehavior!
    let collisionBoundary : UICollisionBehavior!
    var textForTextView : String!
        {
        didSet {
            createView()
        }
    }
   
   
    
    override init(frame: CGRect) {
        self.dynamicAnimator = UIDynamicAnimator()
        self.gravityBehavior = UIGravityBehavior()
        self.collisionBoundary = UICollisionBehavior()
        super.init(frame: frame)
       
        createButtons()
        createOrder()
    }
    func createView() {
        gravityBehavior.gravityDirection = CGVectorMake(0.0, 1.0)
        gravityBehavior.addItem(self)
        backgroundColor = UIColor.tanBackground()
        self.collisionBoundary.addBoundaryWithIdentifier("some", fromPoint: CGPointMake(0, screenRect.height), toPoint: CGPointMake(600, screenRect.height))
        self.collisionBoundary.addItem(self)
        dynamicAnimator.addBehavior(self.collisionBoundary)
        dynamicAnimator.addBehavior(self.gravityBehavior)
        let snapBehavior = UISnapBehavior(item: self, snapToPoint: CGPoint(x: screenRect.width / 2  , y: screenRect.height / 2 ))
        snapBehavior.damping = 0.3
        dynamicAnimator.addBehavior(snapBehavior)
        
        createButtons()
        createOrder()
    }
    
    func createButtons() {
        let cancel = UIButton(frame: CGRectMake(self.frame.width - 35, (screenRect.height / 2) / 18 , 30, 30))
        cancel.setTitle("X", forState: .Normal)
        cancel.addTarget(self, action: "cancel", forControlEvents: .TouchUpInside)
        self.addSubview(cancel)
        
           }

    
    func createOrder() {
        let label = UILabel(frame: CGRectMake(self.frame.origin.x / 2 - 5,  (screenRect.height / 2) / 18, 210, 30))
        let attributedText = NSAttributedString(string: "ITEM ADDED TO CART:", attributes: [NSForegroundColorAttributeName : UIColor.marysRed(), NSUnderlineStyleAttributeName : 1.0, NSFontAttributeName : UIFont.maryBold()])
        label.font = UIFont.maryBold()
        label.attributedText = attributedText
        addSubview(label)
        
        
        
        
        let textView = UITextView(frame: CGRectMake(self.frame.origin.x - 25, label.frame.origin.y + 22, self.frame.width - 15, 150))
        textView.editable = false
        textView.selectable = false
        textView.scrollEnabled = false
        textView.font = UIFont(name: "Cabin-Bold", size: 16)
        textView.backgroundColor = UIColor.clearColor()
        textView.textColor = UIColor.marysGreen()
        textView.text = textForTextView
        addSubview(textView)
        
    }
    func cancel() {
        removeFromSuperview()
    }
   

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
