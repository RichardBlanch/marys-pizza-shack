//
//  Detail+Delegates.swift
//  Mary's Pizza Shack
//
//  Created by Rich Blanchard on 8/13/15.
//  Copyright (c) 2015 Rich. All rights reserved.
//

import Foundation
import UIKit


 extension  DetailMenuController
{
    func giveAdditionalPrice(totalPrice: Double) {
        let updatedPrice = Calculator.getCurrentTextPrice(self.priceLabel , doublePassed : totalPrice)
        let newPrice = CurrencyFormatter.sharedInstance.stringFromNumber(NSNumber(double: updatedPrice))!
        self.priceLabel.text = newPrice
        self.pricesForExtras.append(totalPrice)
        
    }
    func createSaucePickerView() {
        customAlertView = CustomAlertView()
        customAlertView!.delegate = self
        view.addSubview(customAlertView!)
    }
    func giveAdditionalExtras(extras: String) {
        self.arrayForExtras!.append(extras)
    }
    func removeAdditionalPrice(totalPrice: Double) {
        let updatedPrice = Calculator.getCurrentTextPrice(self.priceLabel, doublePassed: totalPrice)
        let newPrice = CurrencyFormatter.sharedInstance.stringFromNumber(NSNumber(double: updatedPrice))!
        self.priceLabel.text = newPrice
        self.pricesForExtras.removeObject((-totalPrice))
       
       
    }
    func removeAdditionalExtras(extras: String) {
        arrayForExtras?.removeAtIndex((arrayForExtras?.indexOf(extras))!)
        
    }
    func showBanner(itemToAdd:CartItem) {
        
        
        let banner = Banner(title: "Item Added:", subtitle: "\(Calculator.getCorrectString(itemToAdd))", image: UIImage(named: "Buy-32"), backgroundColor: UIColor.marysGreen()) { () -> () in
            self.timer.invalidate()
            UIView.transitionWithView(self.view, duration: 1.0, options: UIViewAnimationOptions.TransitionFlipFromTop, animations: { () -> Void in
                self.view.alpha = 0.0
                
            }, completion: { (sucess) -> Void in
                self.goToCart()
            })
        }
        banner.show(self.view, duration: 1.5)
        banner.textColor = UIColor.whiteColor()
        
    }

    func addSauces(sauceToAdd: String) {
        self.sauceToAdd = sauceToAdd
        print("The sauce is \(sauceToAdd)")
        
    }
    func goToExtras() {
        self.change(self.pickerViewButton)
    }
    func goToCart() {
        performSegueWithIdentifier("cartSegue", sender: self)
        
    }
    
    //MARK: TABLE VIEW DATA SOURCE AND DELEGATE
    
     func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        if let _ =  menuItem.item.getItem().Extras
        {
            return 1
        }
        return 0
    }
     func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if let extras =  menuItem.item.getItem().Extras
        {
            return extras.keys.count
        }
        return 0
    }
     func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath) as! ExtrasTableViewCell
        cell.delegate = self
        cell.backgroundColor = UIColor(patternImage: UIImage(named: "background2")!)
        cell.selectionStyle = .None
        cell.count = indexPath.row
        cell.menuItem = menuItem
        return cell
        
    }
    func callTimer() {
        timer = NSTimer.scheduledTimerWithTimeInterval(1.5, target: self, selector: Selector("goBackToMenu"), userInfo: nil, repeats: false)
    }
    func replaceTableViewWithImageView() {
        extrasLabel.hidden = true
        extrasLabel = nil
        let imageView = UIImageView(frame: CGRectMake((screenRect.width / 2) / 2, descriptionTextView.frame.origin.y + descriptionTextView.frame.size.height , screenRect.width / 2, screenRect.height / 2.5))
        //   var randomIndex = Int(Int(arc4random()) % arrayOfImages.count)
        imageView.image = UIImage(named: "mary")
        imageView.contentMode = .ScaleAspectFill
        view.addSubview(imageView)
        
    }
    func setUpAnimations() {
        self.extrasLabel.layer.shadowOffset = CGSizeMake(0, 3)
        self.extrasLabel.layer.shadowRadius = 10.0
        self.extrasLabel.layer.shadowColor = UIColor.blackColor().CGColor
        self.extrasLabel.layer.shadowOpacity = 0.8
        
    }


    func goBackToMenu() { //push user out of menu after order
        self.navigationController?.popViewControllerAnimated(true)
    }
    override func  prefersStatusBarHidden() -> Bool {
    return true
    }
}