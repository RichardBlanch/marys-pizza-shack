//
//  LocationTest.swift
//  Mary's Pizza Shack
//
//  Created by Rich Blanchard on 12/28/15.
//  Copyright © 2015 Rich. All rights reserved.
//

import XCTest
import CoreLocation

class LocationTest: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    func testLatitudeAndLongitudeAndPhoneNumbers() {
        let allRestaurants = Restaurants.getArray()
        let secondCheckLocations = [CLLocationCoordinate2D(latitude: 38.433800, longitude: -122.65052110), // Sonoma Plaza
            CLLocationCoordinate2D(latitude: 40.4352679, longitude: -122.2904319), //Anderson
            CLLocationCoordinate2D(latitude: 38.3127622, longitude: -122.5049721), //Boyes
            CLLocationCoordinate2D(latitude: 38.7834225, longitude: -123.016668), //Cloverdale
            CLLocationCoordinate2D(latitude: 38.4566895, longitude: -121.8460443), //Dixon
            CLLocationCoordinate2D(latitude: 38.3065325, longitude: -122.2827468), //Fairfield
            CLLocationCoordinate2D(latitude: 38.2882765, longitude: -122.4138733), //Napa
            CLLocationCoordinate2D(latitude: 38.1227482, longitude: -122.6052409), //Novato
            CLLocationCoordinate2D(latitude: 38.2537055, longitude: -122.6361802), //Petaluma East
            CLLocationCoordinate2D(latitude: 38.2376674, longitude: -122.6385871), //P west
            CLLocationCoordinate2D(latitude: 40.5868939, longitude: -122.2367987), //Redding
            CLLocationCoordinate2D(latitude: 38.3589598, longitude: -122.7176473), //Ropo
            CLLocationCoordinate2D(latitude: 38.7911983, longitude:-121.2804042), //Roseville
            CLLocationCoordinate2D(latitude: 38.4410032, longitude: -122.7157996), //SR DOWNTOWN
            CLLocationCoordinate2D(latitude: 38.453856, longitude: -122.6737776),//SR EAST
            CLLocationCoordinate2D(latitude: 38.4664889, longitude: -122.7530665), //SR weST
            CLLocationCoordinate2D(latitude: 38.4080883, longitude: -122.829602),//SEBASTOPOL
            CLLocationCoordinate2D(latitude: 38.3534379, longitude: -121.9881855), //vacaville
            CLLocationCoordinate2D(latitude: 37.9309898, longitude: -122.0168306), //walnut creek
            CLLocationCoordinate2D(latitude: 38.5757075, longitude: -122.7985855)] //windsor
        
        
        
         let secondCheckPhoneNumbers = ["(707) 938-8300","(530) 378-1110","(707) 938-3600","(707) 894-8977","(707) 693-0300","(707) 422-2700","(707) 257-3300","(415) 897-6266","(707) 765-1959","(707) 778-7200","(530) 247-1110","(707) 585-3500","(916) 780-7600","(707) 571-1959","(707) 538-1888","(707) 573-1100","(707) 829-5800","(707) 446-7100","(925) 938-4800","(707) 836-0900"]
        
        for var i = 0; i < secondCheckLocations.count; i++ {
            let check =  checkDistance(allRestaurants[i].coordinates.coordinate, coordinateTwo: secondCheckLocations[i])
            XCTAssert(check, "The location with the \(allRestaurants[i].name) is not right")
            XCTAssertEqual(secondCheckPhoneNumbers[i], allRestaurants[i].phoneNumber, "The phone number with \(allRestaurants[i]) is not right)")
            
        }
        
    }
    
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measureBlock {
            // Put the code you want to measure the time of here.
        }
    }
    func checkDistance(coordinateOneActualData:CLLocationCoordinate2D,coordinateTwo:CLLocationCoordinate2D)->Bool {
        let check = fabs(coordinateOneActualData.latitude - coordinateTwo.latitude) <= 0.2 && fabs(coordinateOneActualData.longitude - coordinateTwo.longitude) <= 0.2
        return check
    }
    
}
