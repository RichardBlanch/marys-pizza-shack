//
//  Resturant.swift
//  
//
//  Created by Rich Blanchard on 8/13/15.
//
//

import Foundation
import UIKit
import CoreLocation
import MapKit

class Restaurant  : MKPointAnnotation {
    let name: String!
    var distance: CLLocationDistance!
    let phoneNumber: String!
    let hours: String!
    let coordinates: CLLocation!
    let address: String!
   
    
    init(name:String, phoneNumber:String, hours:String, coordinates: CLLocation, address: String) {
        self.name = name
        self.phoneNumber = phoneNumber
        self.hours = hours
        self.coordinates = coordinates
        self.address = address
    }
    public func getLocationDistance(location: CLLocation, location2: CLLocation)->CLLocationDistance {
        
        let distance = location.distanceFromLocation(location2)
        return distance;
    }
    
}

