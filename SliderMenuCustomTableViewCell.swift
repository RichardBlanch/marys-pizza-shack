//
//  SliderMenuCustomTableViewCell.swift
//  Mary's Pizza Shack
//
//  Created by Rich Blanchard on 8/12/15.
//  Copyright (c) 2015 Rich. All rights reserved.
//

import UIKit

class SliderMenuCustomTableViewCell: UITableViewCell {

    @IBOutlet weak var labelForMenu: UILabel!
    @IBOutlet weak var iconImageView: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
       
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

}
