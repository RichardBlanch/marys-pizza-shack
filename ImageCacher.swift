//
//  ImageCacher.swift
//  Mary's Pizza Shack
//
//  Created by Rich Blanchard on 8/14/15.
//  Copyright (c) 2015 Rich. All rights reserved.
//

import Foundation
class imageCacher : NSObject {
    var cache = NSCache()
    var urls = Set<String>()
    
    override init() {
        super.init()
    }
    class var shardCacher: imageCacher {
        struct Static {
            static let instance = imageCacher()
        }
        return Static.instance
    }
    
}

