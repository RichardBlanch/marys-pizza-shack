//
//  CartCustomTableViewCell.swift
//  Mary's Pizza Shack
//
//  Created by Rich Blanchard on 8/12/15.
//  Copyright (c) 2015 Rich. All rights reserved.
//

import UIKit

class CartCustomTableViewCell: UITableViewCell {

    @IBOutlet weak var itemLabel: UILabel!
    @IBOutlet weak var testLabel: UILabel!
    
    var cartItem : CartItem! {
        didSet {
            let itemAttributedText = NSAttributedString(string: cartItem.item, attributes: [NSFontAttributeName : UIFont.maryBoldSmall(), NSForegroundColorAttributeName : UIColor.marysGreen()])
            
            let priceAttributedText = NSAttributedString(string: cartItem.price, attributes: [NSFontAttributeName : UIFont.maryBoldSmall(), NSForegroundColorAttributeName : UIColor.marysRed()])

            itemLabel.attributedText = itemAttributedText
            testLabel.attributedText = priceAttributedText
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
