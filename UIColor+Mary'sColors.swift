//
//  UIColor+Mary'sColors.swift
//  Mary's Pizza Shack
//
//  Created by Rich Blanchard on 8/11/15.
//  Copyright (c) 2015 Rich. All rights reserved.
//

import Foundation
import UIKit

extension  UIColor {
    class func marysRed()->UIColor
    {
        return  UIColor(red: 37/255, green: 77/255, blue: 37/255, alpha: 1.0)
    }
    class func tanBackground()->UIColor
    {
        return UIColor(patternImage: UIImage(named: "background2")!)
    }
    class func yellowGifBackground()->UIColor
    {
        return UIColor(patternImage: UIImage(named: "yellow")!)
    }
    class func marysGreen()->UIColor
    {
        return UIColor(red: 182/255, green: 38/255, blue: 38/255, alpha: 1.0)
    }
    class func marysGreenBackground()->UIColor
    {
        return UIColor(red: 35/255, green: 84/255, blue: 47/255, alpha: 1.0)
    }
    class func sliderBackgroundColor()->UIColor
    {
        return UIColor(red: 45/255, green: 57/255, blue: 51/255, alpha: 1.0)
    }
    class func coolLightGray()->UIColor {
        return UIColor(red: 34/255, green: 43/255, blue: 38/255, alpha: 1.0)
    }
    class func yellowMarysTabBarColor()->UIColor {
        return UIColor(red: 254/255, green: 236/255, blue: 155/255, alpha: 1.0)
    }
    
}
