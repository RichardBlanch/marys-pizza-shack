//
//  CustomAlertView.swift
//  Mary's Pizza Shack
//
//  Created by Rich Blanchard on 8/11/15.
//  Copyright (c) 2015 Rich. All rights reserved.
//

import UIKit
protocol takeToSauces
{
    func goToExtras()
}

class CustomAlertView: UIView
{
    var animator : UIDynamicAnimator!
    var delegate: takeToSauces?
    

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
    }
    
    override init(frame: CGRect) {
        self.animator = UIDynamicAnimator()
        super.init(frame: frame)
        createView()
        let snap = UISnapBehavior(item: self, snapToPoint: CGPointMake(screenRect.midX, screenRect.midY))
        self.animator.addBehavior(snap)
    }

    func createView() {
        self.frame = CGRect(x: 0, y: 0, width: 250, height: 250)
        backgroundColor = UIColor.marysGreen()
        layer.cornerRadius = 10
        layer.shadowColor = UIColor.blackColor().CGColor
        layer.shadowOffset = CGSize(width: 10, height: 10)
        layer.shadowOpacity = 1.0
        layer.shadowRadius = 2.0
        createButtons()
        createTextView()
    }
    func createButtons() {
        var cancel = UIButton(frame: CGRectMake(self.frame.width - 50, self.frame.origin.y, 30, 30))
        cancel.setTitle("X", forState: .Normal)
        cancel.addTarget(self, action: "cancel", forControlEvents: .TouchUpInside)
        self.addSubview(cancel)
        
        var okay = UIButton(frame: CGRectMake(self.frame.width / 2 - 100, self.frame.height - 75, 200, 50))
        if let font = UIFont(name: "Cabin-Bold", size: 20)
        {
        let colour = UIColor.marysRed()
        var attributes: [NSString : NSObject] = [NSFontAttributeName: font, NSForegroundColorAttributeName: colour]
        

       
        var string = NSAttributedString(string: "Take me there", attributes: [NSFontAttributeName: font, NSForegroundColorAttributeName: colour])
        
        okay.setTitle("Take Me There!", forState: .Normal)
        okay.setAttributedTitle(string, forState: .Normal)
        okay.setTitleColor(UIColor.marysRed(), forState: .Normal)
        okay.backgroundColor = UIColor.whiteColor()
        okay.layer.cornerRadius = 5.0
        okay.addTarget(self, action: "goToSauces", forControlEvents: .TouchUpInside)
        addSubview(okay)
        }
    }
    func createTextView() {
        let textView = UITextView(frame: CGRectMake(self.frame.origin.x + 10, self.frame.height / 2 - 50, self.frame.width - 15, 100))
        textView.editable = false
        textView.selectable = false
        textView.font = UIFont(name: "Cabin-Bold", size: 18)
        textView.backgroundColor = UIColor.clearColor()
        textView.textColor = UIColor.blackColor()
        textView.text = "You are missing a needed input to order this item. Please be sure to fill it out."
        addSubview(textView)
    }
    
    
    func cancel() {
        removeFromSuperview()
        self.animator.removeAllBehaviors()
        self.animator = nil
    }
    func goToSauces() {
        removeFromSuperview()
        self.delegate?.goToExtras()
    }

  
    

}
