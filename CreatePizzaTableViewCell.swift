//
//  CreatePizzaTableViewCell.swift
//  Mary's Pizza Shack
//
//  Created by Rich Blanchard on 8/16/15.
//  Copyright (c) 2015 Rich. All rights reserved.
//

import UIKit


class CreatePizzaTableViewCell: UITableViewCell {
    @IBOutlet weak var addToppingSwitch: UISwitch!
    var delegate: addTopping?
    var topping : PizzaWithTopping!
   
       
    
    
    
    @IBOutlet weak var toppingLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
    }
 
    
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        selectionStyle = .None
        backgroundColor = UIColor.yellowGifBackground()
       
        
    }
    
    @IBAction func switchToggled(sender: UISwitch) {
         let tableViewCell = sender.superview?.superview as! CreatePizzaTableViewCell
        
        if(sender.on){
        tableViewCell.topping.on = true
        delegate?.addTopping(tableViewCell.topping)
        }
        else {
            tableViewCell.topping.on = false
            delegate?.removeTopping(tableViewCell.topping)
            
        }
       
    }
    
}
