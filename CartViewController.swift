//
//  CartViewController.swift
//  Mary's Pizza Shack
//
//  Created by Rich Blanchard on 8/12/15.
//  Copyright (c) 2015 Rich. All rights reserved.
//

import UIKit


class CartViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var toggler: UIButton!
    @IBOutlet weak var imageBackgroundView: UIView!
    @IBOutlet weak var tableView: UITableView!
    var navBar : UINavigationBar!
    var cartItemToSegue: CartItem?
    var order : String!
    
    override func viewWillAppear(animated: Bool) {
        navigationController?.navigationBar.hidden = true
        navBar = UINavigationBar(frame: CGRectMake(0, 0, screenRect.width, 70))
        self.view.addSubview(navBar)
        
        tabBarController?.tabBar.hidden = true
        setUpBackButtonAndCheckOutButton()
        
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        imageBackgroundView.backgroundColor = UIColor.tanBackground()
        self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 2
    }
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(section == 0)
        {
            return Cart.sharedCart.items.count
        }
        else {
            return 1
        }
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        if(indexPath.section == 0)
        {
            let cell = tableView.dequeueReusableCellWithIdentifier("cell", forIndexPath: indexPath) as! CartCustomTableViewCell
            cell.cartItem =  Cart.sharedCart.items[indexPath.row]
            cell.selectionStyle = .None
            return cell
        }
        else {
            let cell = tableView.dequeueReusableCellWithIdentifier("total", forIndexPath: indexPath) 
            cell.detailTextLabel!.text = Cart.sharedCart.calculateTotal(Cart.sharedCart.items)
            cell.selectionStyle = .None
            
            return cell
            
        }
        
    }
    func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        if(indexPath.section == 0)
        {
            if(Cart.sharedCart.items.count > 0)
            {
                return true
            }
            return false
        }
        else {
            return false
        }
    }
    func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == UITableViewCellEditingStyle.Delete {
            tableView.beginUpdates()
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Automatic)
            Cart.sharedCart.items.removeAtIndex(indexPath.row)
            tableView.endUpdates()
            tableView.reloadData()
        }
    }
    func tableView(tableView: UITableView, accessoryButtonTappedForRowWithIndexPath indexPath: NSIndexPath) {
        
        cartItemToSegue = Cart.sharedCart.items[indexPath.row]
        performSegueWithIdentifier("goToDetailDisclosure", sender: self)
    }
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "goToDetailDisclosure" {
            let detailVC = segue.destinationViewController as! CartDetailDisclosureViewController
            detailVC.cartItem = cartItemToSegue!
        }
        else if segue.identifier == "goToCheckOut" {
            let vc = segue.destinationViewController as! OrderViewController
            let priceString = Cart.sharedCart.calculateTotal(Cart.sharedCart.items) as NSString
          let newPriceString = priceString.stringByReplacingOccurrencesOfString("$", withString: "", options: NSStringCompareOptions.LiteralSearch, range: priceString.rangeOfString(priceString as String)) as NSString
            let totalPrice = NSDecimalNumber(double: newPriceString.doubleValue)
            vc.totalPrice = totalPrice
        
           
        }
    }
    func setUpBackButtonAndCheckOutButton() {
        let navItem = UINavigationItem()
        navItem.title = "Cart"
        let backButton = UIBarButtonItem()
        backButton.image = UIImage(named: "More-25")
        backButton.tintColor = UIColor.marysRed()
        navigationItem.leftBarButtonItem = backButton
        backButton.target = self.revealViewController()
        backButton.action = Selector("revealToggle:")
        
        navItem.leftBarButtonItem = backButton
        
        let goToCheckout = UIBarButtonItem()
       goToCheckout.image = UIImage(named: "Checked Checkbox 2-25")
        goToCheckout.tintColor = UIColor.marysGreen()
        goToCheckout.target = self
        goToCheckout.action = Selector("goToCheckOut")
        navItem.rightBarButtonItem = goToCheckout
        
        
        navBar.items = [navItem]
    }
    override func  prefersStatusBarHidden() -> Bool {
    return true
    }
    func goToCheckOut() {
        performSegueWithIdentifier("goToCheckOut", sender: self)
    }
}
