//
//  UIFont+Mary'sFonts.swift
//  Mary's Pizza Shack
//
//  Created by Rich Blanchard on 8/11/15.
//  Copyright (c) 2015 Rich. All rights reserved.
//

import Foundation
import UIKit


extension UIFont
{
    class func maryBold() -> UIFont {
        return UIFont(name: "Cabin-Bold", size: 20)!
    }
    class func maryBoldSmall() -> UIFont {
        return UIFont(name: "Cabin-Bold", size: 14)!
    }
}
