//
//  PizzaWithTopping.swift
//  Mary's Pizza Shack
//
//  Created by Rich Blanchard on 8/16/15.
//  Copyright (c) 2015 Rich. All rights reserved.
//

import Foundation

class PizzaWithTopping : NSObject {
    var on : Bool!
    var topping: String!
     init(on:Bool, topping:String) {
        self.on = on
        self.topping = topping
        super.init()
    }
}