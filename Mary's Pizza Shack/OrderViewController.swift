//
//  OrderViewController.swift
//  Mary's Pizza Shack
//
//  Created by Rich Blanchard on 8/15/15.
//  Copyright (c) 2015 Rich. All rights reserved.
//

import UIKit
import MessageUI
import PassKit
var globalTotal: Double?
class OrderViewController: UIViewController,UITextFieldDelegate, MFMailComposeViewControllerDelegate, UINavigationControllerDelegate, UITextViewDelegate, PKPaymentAuthorizationViewControllerDelegate {
    @IBAction func useVenmo(sender: UIButton) {
        let venmoCash = UInt(getFormattedNumber(globalTotal!))
        Venmo.sharedInstance().sendPaymentTo("rich-blanchard", amount: venmoCash, note: "Mary's Meal", audience: VENTransactionAudience.UserDefault) { (transaction, success, error) -> Void in
            
            if(success) {
                print("This worked")
                
            }
            if ((error) != nil) {
                print("The error is \(error.localizedDescription)")
                
            }
        }

    }
    
  
    @IBOutlet weak var nameTextfield: UITextField!
    @IBOutlet weak var creditCardNumberTextfield: UITextField!
    @IBOutlet weak var addressTextfield: UITextField!
    @IBOutlet weak var suggestionsTextview: UITextView!
    @IBOutlet weak var expirationDate: UITextField!
    var totalPrice : NSDecimalNumber!
    var paymentTextField : STPPaymentCardTextField?    
    @IBOutlet weak var applePayButton: UIButton!
    let defaults = NSUserDefaults.standardUserDefaults()

    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        view.backgroundColor = UIColor.tanBackground()
        
        let totalPriceFromCart = Cart()
        print("\(totalPriceFromCart.currentPrice)\n")
        
       var amountText = "\(totalPrice)"
        globalTotal = Double(amountText)
        globalTotal = globalTotal! * 100
      //  hideWhatYouNeedTo()
    }

//    func hideWhatYouNeedTo() {
//        switch(String(self.defaults.objectForKey("PaymentMethod"))) {
//            default :
//                for smallerView in self.view.subviews {
//                    self.applePayButton.hidden = true
//                    for viewer in self.view.subviews {
//                        if(viewer.classForCoder == UIButton.classForCoder()) {
//                            viewer as! UIButton
//                            if(viewer.currentBackgroundImage == "m")
//                        }
//                        }
//                    }
//            }
//            
//            
//        }
//    }
    
    
    
    @IBAction func back(sender: UIButton) {
        dismissViewControllerAnimated(true, completion: nil)
    }
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        return textField.resignFirstResponder()
    }
    func textViewShouldEndEditing(textView: UITextView) -> Bool {
        return textView.resignFirstResponder()
    }
    
    
    @IBAction func order(sender: AnyObject) {
        let card = STPCard()
        card.number = creditCardNumberTextfield.text
        if !expirationDate.text!.isEmpty {
            if expirationDate.text!.rangeOfString("/") == nil{
               let alertView = UIAlertView(title: "Please Enter a Valid Expiration Date", message: "Please enter in the form MM/YY", delegate: nil, cancelButtonTitle: "Ok")
                alertView.show()
            }
            else {
        let expire = expirationDate.text!.componentsSeparatedByString("/")
        let expMonth = Int(expire[0])
        let expYear = Int(expire[1])
        card.expMonth = UInt(expMonth!)
        card.expYear = UInt(expYear!)
            }

            
        STPAPIClient.sharedClient().createTokenWithCard(card, completion: { (token, error) -> Void in
            if (error != nil) {
                var alert = UIAlertView(title: "Error!", message: "\(error!.localizedDescription)", delegate: nil, cancelButtonTitle: "Ok")
                alert.show()
            }
            else {
            createBackendChargeWithToken(token!, completion: { (status) -> Void in
                
            });
            let mailComposeViewController = self.configuredMailComposeViewController()
            if MFMailComposeViewController.canSendMail() {
                self.presentViewController(mailComposeViewController, animated: true, completion: nil)
            } else {
                var alertView = UIAlertView(title: "You are unable to send an email", message: "Please Try again later", delegate: nil, cancelButtonTitle: "Ok")
                alertView.show()
            }
            }
        })
        
        
        }
        
        
        
        
        
        
       
    }
    func configuredMailComposeViewController() -> MFMailComposeViewController {
        let mailComposerVC = MFMailComposeViewController()
        mailComposerVC.mailComposeDelegate = self // Extremely important to set the -
        var mutableBodyString = Calculator.getOrder(Cart.sharedCart.items)
        mutableBodyString += ("\n")
        

        for var i = 0; i < view.subviews.count ; i++
        {
            
            
            if let textField = self.view.subviews[i] as? UITextField
            {
                if textField == creditCardNumberTextfield || textField == expirationDate {continue}
                mutableBodyString += textField.text!
                mutableBodyString += "\n"
            }
            else if let suggestions = self.view.subviews[i] as? UITextView {
                mutableBodyString += suggestions.text
                mutableBodyString += "\n"
                
            }
        }
        
        
        
        mailComposerVC.setToRecipients(["MarysPizzaShack@aim.com"])
        mailComposerVC.setSubject("Your Order")
        mailComposerVC.setMessageBody(mutableBodyString, isHTML: false)
        
        
        
        return mailComposerVC
    }
    func textViewShouldBeginEditing(textView: UITextView) -> Bool {
        textView.text = ""
        return true
    }
   override func  prefersStatusBarHidden() -> Bool {
    return true
    }
    
    
    // MARK: MFMailComposeViewControllerDelegate Method
    func mailComposeController(controller: MFMailComposeViewController, didFinishWithResult result: MFMailComposeResult, error: NSError?) {
        controller.dismissViewControllerAnimated(true, completion: nil)
    }
    
    @IBAction func applePaySelected(sender: UIButton) {
        
        var request = Stripe.paymentRequestWithMerchantIdentifier("merchant.testerMary")
      
        
        request?.paymentSummaryItems = [PKPaymentSummaryItem(label: "Your meal", amount:totalPrice)]
        request?.requiredBillingAddressFields = PKAddressField.PostalAddress
        request?.requiredBillingAddressFields = PKAddressField.PostalAddress
        
        
        
       
        
       
       
    var paymentController = PKPaymentAuthorizationViewController(paymentRequest: request!)
    paymentController.delegate = self
    presentViewController(paymentController, animated: true, completion: nil)
        
    }
    func paymentAuthorizationViewController(controller: PKPaymentAuthorizationViewController, didSelectShippingAddress address: ABRecord, completion: (PKPaymentAuthorizationStatus, [PKShippingMethod], [PKPaymentSummaryItem]) -> Void) {
        let array = ShippingManager.defaultShippingMethods()
        
        
    }
    func paymentAuthorizationViewController(controller: PKPaymentAuthorizationViewController, didAuthorizePayment payment: PKPayment, completion: ((PKPaymentAuthorizationStatus) -> Void)) {
       STPAPIClient.sharedClient().createTokenWithPayment(payment) { (token, error) -> Void in
        createBackendChargeWithToken(token!, completion: { (status) -> Void in
            if(status == STPBackendChargeResult.Success) {
                completion(PKPaymentAuthorizationStatus.Success)
                print("Sucess")
            }
            else {
                completion(PKPaymentAuthorizationStatus.Failure);
                 print("Failutre")
            }
        })
        }
       
    }
    
    
    func paymentAuthorizationViewControllerDidFinish(controller: PKPaymentAuthorizationViewController) {
        controller.dismissViewControllerAnimated(true, completion: nil)
    }

}
    
func handlePaymentAuthorizationWithPayment(payment : PKPayment, completion: (status: PKPaymentAuthorizationStatus) -> Void) {
    STPAPIClient.sharedClient().createTokenWithPayment(payment) { (token, error) -> Void in
        if(error != nil) {
            completion(status: PKPaymentAuthorizationStatus.Failure)
        }
      
    }
}

func paymentCardTextFieldDidChange(textField: STPPaymentCardTextField, completion: (status: PKPaymentAuthorizationStatus) -> Void) {
    print("Something happened man")
}
func createBackendChargeWithToken(token: STPToken,completion:(status: STPBackendChargeResult) -> Void){
    let config = NSURLSessionConfiguration.defaultSessionConfiguration()
    let session = NSURLSession(configuration: config)
    var urlString = backEndChargeURLString
    let correctAmount = getFormattedNumber(globalTotal!)
   
    var appendedString = "?stripeToken=\(token.tokenId)&amount=\(correctAmount)"
   let finalString =  urlString.stringByAppendingString(appendedString)
   
    let url = NSURL(string: finalString)
    let request = NSMutableURLRequest(URL: url!)
    request.HTTPMethod = "POST"
    let postBody = NSString(string: "stripeToken=\(token.tokenId)")
    let data = postBody.dataUsingEncoding(NSUTF8StringEncoding)
    let uploadTask = session.uploadTaskWithRequest(request, fromData: data) { (data, response, error) -> Void in
        if(error != nil) {
        print("Error is \(error?.localizedDescription)")
         completion(status: STPBackendChargeResult.Failure);
        print("Failed")
    }
        else {
            completion(status: STPBackendChargeResult.Success);
            
            print("Success")
            }
    }
    uploadTask.resume()

}
func getFormattedNumber(doubleToConvert: Double)->Int {
    let myIntValue = Int(doubleToConvert)
    return myIntValue
}








