//
//  ShippingManager.swift
//  Mary's Pizza Shack
//
//  Created by Rich Blanchard on 12/22/15.
//  Copyright © 2015 Rich. All rights reserved.
//

import Foundation
class ShippingManager {

class func defaultShippingMethods() ->NSArray {
    return californiaShippingMethods()
}

class func californiaShippingMethods()->NSArray {
    var normalItem = PKShippingMethod(label:"LLama California Shipping", amount: NSDecimalNumber(string: "20.00"))
    normalItem.detail = "3-5 business days"
    normalItem.identifier = normalItem.label
    var expressItem = PKShippingMethod(label: "LLama California Chipping", amount: NSDecimalNumber(string: "25.00"))
    expressItem.detail = "Next day"
    expressItem.identifier = expressItem.label
    return [normalItem,expressItem]
}
}