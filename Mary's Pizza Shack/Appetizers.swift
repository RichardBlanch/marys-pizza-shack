//
//  Appetizers.swift
//  Mary's Pizza Shack
//
//  Created by Rich Blanchard on 8/6/15.
//  Copyright (c) 2015 Rich. All rights reserved.
//

import Foundation
import UIKit
class Appetizer : MenuItem
    
{
     class func getItemsAndImage() -> (array:[Appetizer] , image: UIImage ) {
        let items = [Appetizer(item: .Breadsticks), Appetizer(item: .FrescoPestoBreadSticks), Appetizer(item: .MozzarellaGarlicBread), Appetizer(item: .HeandShe), Appetizer(item: .CalamartiFritti), Appetizer(item: .ChickenWings), Appetizer(item: .ChickenStrips), Appetizer(item: .ShrimpBruschetta), Appetizer(item: .Bruschetta), Appetizer(item: .SimplePasta), Appetizer(item: .SauteedVegetables), Appetizer(item: .MarysFrenchFries), Appetizer(item: .GarlicFries)]
        let image = UIImage(named: "appetizers_real")
        
        return (array: items, image:image!)
    }
}
