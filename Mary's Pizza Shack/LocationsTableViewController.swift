//
//  LocationTableViewController.swift
//  Mary's Pizza Shack
//
//  Created by Rich Blanchard on 8/14/15.
//  Copyright (c) 2015 Rich. All rights reserved.
//

import UIKit
import CoreLocation

class LocationTableViewController: UIViewController, UITabBarControllerDelegate, UITableViewDataSource {
    @IBOutlet weak var tableView: UITableView!
    var arrayOfResturants : [Restaurant]!
    var location : CLLocation!
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(true)
        tabBarController?.tabBar.hidden = true
        addBackButton()
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = UIColor.tanBackground()
        self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        self.tableView.backgroundColor = UIColor.tanBackground()
        location = CLLocation()
        location = locationHandler.sharedManager.location
        
        arrayOfResturants = Restaurants.getArray() //get all our restuarants
        for var i = 0; i < arrayOfResturants.count; i++ {
            var rest = arrayOfResturants[i] as! Restaurant
            var distance = rest.getLocationDistance(location, location2: rest.coordinates)
            distance = distance / 1609.34
            rest.distance = distance
            print("The distance is \(distance)")
        }
        arrayOfResturants = Restaurants.sortArrayByDistanceFromCurrentLocation(arrayOfResturants)
    }
    
    override func prefersStatusBarHidden() -> Bool {
        return true
    }
    
    // MARK: - Table view data source
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Potentially incomplete method implementation.
        // Return the number of sections.
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete method implementation.
        // Return the number of rows in the section.
        return arrayOfResturants.count
    }
    
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath) as! LocationsTableViewCell
        cell.location = self.location
        cell.resturant = arrayOfResturants[indexPath.row]
        cell.backgroundColor = UIColor.coolLightGray()
       
        return cell
    }
    func addBackButton() {
       
        let backButton = UIBarButtonItem()
        backButton.image = UIImage(named: "More-25")
        backButton.tintColor = UIColor.marysRed()
        navigationItem.leftBarButtonItem = backButton
        backButton.target = self.revealViewController()
        backButton.action = Selector("revealToggle:")
        self.navigationItem.leftBarButtonItem = backButton
        
    }
    
}
