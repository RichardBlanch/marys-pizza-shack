//
//  Appetizers.swift
//  Mary's Pizza Shack
//
//  Created by Rich Blanchard on 8/6/15.
//  Copyright (c) 2015 Rich. All rights reserved.
//

import Foundation
import UIKit

class Salad : MenuItem
    
    
{
     class func getItemsAndImage() -> (array:[Salad] , image: UIImage )
    {
        let saladArray = [Salad(item: .HomemadeSoupoftheDay), Salad(item: .MarysSignatureSalad), Salad(item: .CaesarSalad), Salad(item: .SpinachSalad), Salad(item: .ItalianChoppedSalad), Salad(item: .TheWedge), Salad(item: .CobbSalad), Salad(item: .SoupandSaladCombo)]
        
        let soupsAndSaladsImage = UIImage(named: "soups_and_salads_button")
        
        return (array:saladArray, image:soupsAndSaladsImage!)
    }
  
    
 
}

