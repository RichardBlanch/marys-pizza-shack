//
//  Cart.swift
//  Mary's Pizza Shack
//
//  Created by Rich Blanchard on 8/11/15.
//  Copyright (c) 2015 Rich. All rights reserved.
//

import Foundation

class Cart : NSObject {
    var items = [CartItem]()
    var currentPrice:Double!

     override init() {
        super.init()
           }
    class var sharedCart: Cart {
             struct Static {
            static let instance = Cart()
            
        }
        return Static.instance
    }
    func addItem(item:CartItem) {
        self.items.append(item)
    }
    func calculateTotal(orderedItems: [CartItem]) -> String
    {
        var total:Double = 0.0
        var totalAsString:String!
        for item in orderedItems {
            let choppedString = item.price as NSString
            let newString = choppedString.stringByReplacingOccurrencesOfString("$", withString: "") as NSString!
            let price = newString.doubleValue
            total += price
            
        }
        self.currentPrice = total
        totalAsString = CurrencyFormatter.sharedInstance.stringFromNumber(NSNumber(double: total))
        return totalAsString
        
    }
}