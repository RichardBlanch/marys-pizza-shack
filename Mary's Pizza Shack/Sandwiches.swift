//
//  Sandwiches.swift
//  Mary's Pizza Shack
//
//  Created by Rich Blanchard on 8/8/15.
//  Copyright (c) 2015 Rich. All rights reserved.
//

import Foundation
import UIKit

class Sandwiches : MenuItem
{
    class func getItemsAndImages() -> (array: [Sandwiches], image:UIImage)
    {
        let items = [Sandwiches(item: .BuffaloChickenWrap),Sandwiches(item: .MarysOriginalPizzaDoughHotDog), Sandwiches(item: .MeatballSandwich), Sandwiches(item: .ChickenAndBeaconClub), Sandwiches(item: .TurkeySupremo) , Sandwiches(item: .EggplantAndArtichokeSandwich) , Sandwiches(item: .ItalianBLTA), Sandwiches(item: .GrilledChickenSandwich) , Sandwiches(item: .MarysFamousHamburger)]
        
        let image = UIImage(named: "sandwiches_real")
        
        
        return (array: items, image: image!)
        
    }
    
}