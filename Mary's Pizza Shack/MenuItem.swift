//
//  MenuItem.swift
//  Mary's Pizza Shack
//
//  Created by Rich Blanchard on 8/7/15.
//  Copyright (c) 2015 Rich. All rights reserved.
//

import Foundation
import UIKit

public let screenRect = UIScreen.mainScreen().bounds
var timer: NSTimer!

public let arrayOfImages = [UIImage(named: "mary"), UIImage(named: "maryhistory")]
class MenuItem
{
    var image:UIImage!
    var item:itemType!
    var sorters:[Sort]!
    
    
    
    init(item: itemType)
    {
        self.item = item
        
    }
    
    enum itemType {
        
        //Appetizers
        case Breadsticks
        case FrescoPestoBreadSticks
        case MozzarellaGarlicBread
        case HeandShe
        case CalamartiFritti
        case ChickenWings
        case ChickenStrips
        case ShrimpBruschetta
        case Bruschetta
        case SimplePasta
        case SauteedVegetables
        case MarysFrenchFries
        case GarlicFries
        //Soups And Salads
        case HomemadeSoupoftheDay
        case MarysSignatureSalad
        case CaesarSalad
        case SpinachSalad
        case ItalianChoppedSalad
        case TheWedge
        case CobbSalad
        case SoupandSaladCombo
        
        //Sandwiches
        case MarysOriginalPizzaDoughHotDog
        case MeatballSandwich
        case ChickenAndBeaconClub
        case TurkeySupremo
        case EggplantAndArtichokeSandwich
        case ItalianBLTA
        case GrilledChickenSandwich
        case MarysFamousHamburger
        case BuffaloChickenWrap
        case ChickenCaesarWrap
        case TurkeyWrap
        
        //Pastas and Entrees
        case SpaghettiWithAGiantMeatball
        case GnoochiWithAGiantMeatball
        case HeAndShe
        case LinguineMarinara
        case PennewithSausage
        case ProteinPlate
        case ChickenFettuccineAlfredo
        case ChickenPicatta
        case MeatRaviolli
        case CheeseRaviolli
        case TortelliniallaMarys
        case MarysClassicLasagna
        case ChickenParmigiana
        case EggplantParmigiana
        
        //Desserts
        case BlueberryCrumbleCheesecake
        case MarysOriginalMudPie
        case SaltedCaramelGelatoPie
        case GiganticCookieSundae
        case MarysMessyBrownieSundae
        case PineappleUpsideDownCake
        
        //Mediteranean Menu
        case BurrataAndArugula
        case TunaAndCouscous
        case RigatoniWithArtichokes
        case PestoPomodoro
        case LinguineWithClams
        case LemonShrimpLinguine
        case PastaConTonno
        case ItalianFarmersMarket
        
        //Pizzas
        case MarysCombinations
        case TotosCombo
        case HawaiianLuau
        case BuffaloChicken
        case TuscanGarlicChicken
        case PestoChicken
        case BBQChicken
        case PizzaAlaSriracha
        case Napoletana
        case ClassicVegetarian
        case LaRomano
        case ClassicCheeseCalzone
        case VegetarianCalzone
        case MarysMeatCalzone
        
        
        
        
        
        
        
        
        
        func getItem()->(itemName:String, itemDescription:String, defaultPrice: Double , prices:[String : Double]?, sauces:[String]?, Extras: [String: Double]?)
        {
            switch self {
                
                
                //APPETIZERS
            case .Breadsticks:
                return(itemName: "Bread Sticks", itemDescription: "Mary’s famous pizza dough bread sticks served with butter, homemade pizza sauce or ranch dressing.", defaultPrice: 4.75 , prices:nil, sauces:nil, Extras: nil)
                
            case .FrescoPestoBreadSticks:
                return(itemName: "Fresco Pesto Bread Sticks", itemDescription: "Mary’s famous pizza dough twists with fresh basil pesto and asiago cheese, baked to perfection. Dip them in homemade ranch dressing.", defaultPrice: 7.25 , prices:nil, sauces:nil, Extras: nil)
                
            case .MozzarellaGarlicBread:
                return(itemName: "Mozzarella Garlic Bread", itemDescription: "Sonoma sourdough topped with garlic butter, oregano, paprika and mozzarella cheese, melted to perfection.", defaultPrice: 4.75 , prices:["Regular Order" : 4.75, "Half Loaf" : 8.95, "Loaf" : 13.75 ], sauces:nil, Extras: nil)
            case .HeandShe:
                return(itemName: "\"He & She\"", itemDescription: "A Mary's original, two meatballs topped with our old world meat sauce and plenty of melted mozzarella. ", defaultPrice: 4.75 , prices:nil, sauces:nil, Extras: nil)
                
            case .CalamartiFritti:
                return(itemName: "Calamari Fritti", itemDescription: "Crispy, lightly breaded calamari served with your choice of tartar or cocktail sauce. ", defaultPrice: 10.75 , prices:nil, sauces:nil, Extras: nil)
                
            case .ChickenWings:
                return(itemName: "Chicken Wings", itemDescription: "Crispy chicken wings tossed with your choice of homemade sauces served with blue cheese or ranch dressing.", defaultPrice: 9.75 , prices:nil, sauces:["Sriracha Sweet Chili Sauce", "Lemon-Herb Sauce Spicy","Buffalo Sauce", " Sweet-and-Spicy Barbecue Sauce "], Extras: nil)
                
            case .ChickenStrips:
                return(itemName: "Chicken Strips", itemDescription: "Lightly breaded chicken strips with your choice of blue cheese dressing, ranch dressing or barbecue sauce.", defaultPrice: 7.75 , prices:nil, sauces:nil, Extras: nil)
                
            case .ShrimpBruschetta:
                return(itemName: "Shrimp Bruschetta", itemDescription: "Toasted slices of sourdough topped with shrimp in a garlic lemon and creamy white wine sauce. Topped with garlic, parmesan and parsley.", defaultPrice: 9.50 , prices:["Four Pieces" : 9.50, "Six Pieces" : 12.50], sauces:nil , Extras: nil)
                
            case .Bruschetta:
                return(itemName: "Bruschetta", itemDescription: "Toasted sourdough topped with diced fresh tomatoes, garlic, basil and olive oil. ", defaultPrice: 7.25 , prices:["Bruschetta" : 9.50, "w/ Feta" : 10.50],  sauces:nil , Extras: nil)
                
            case .SimplePasta:
                return(itemName: "Simple Pasta", itemDescription: "Spaghetti, rigatoni, penne or linguine with your choice of meat, marinara or pesto sauce.", defaultPrice: 8.25 , prices:nil, sauces:nil , Extras: nil)
                
            case .SauteedVegetables:
                return(itemName: "Sautéed Vegetables", itemDescription: "Fresh zucchini, mushrooms, spinach, red onions and roasted red peppers sautéed with garlic", defaultPrice: 4.25 , prices:nil, sauces:nil , Extras: nil)
                
            case .MarysFrenchFries:
                return(itemName: "Mary’s French Fries", itemDescription: "Mary’s crispy french fries. ", defaultPrice: 3.25 , prices:["Small" : 3.25, "Large" : 4.75], sauces:nil, Extras: nil)
                
            case .GarlicFries:
                return(itemName: "Garlic Fries", itemDescription: "Crispy fries topped with fresh garlic, chopped parsley and parmesan.", defaultPrice: 4.25 , prices:["Small" : 4.25, "Large" : 6.95], sauces:nil , Extras: nil)
                
                
                
                
                
                
                
                
                
                //SOUPS AND SALADS
            case .HomemadeSoupoftheDay:
                
                return(itemName: "Homemade Soup of the Day", itemDescription: "Made from Mary’s own family recipes, and changing with the day and the season.  Served with fresh bread.", defaultPrice: 3.00 , prices:["Cup" : 3.00, "Bowl" : 5.95], sauces: ["Sonoma Italian","Thousand Island","Blue Cheese","Ranch","Tamari","Non-fat Raspberry Vinaigrette"] , Extras: nil)
                
            case .MarysSignatureSalad:
                
                return(itemName: "Mary’s Signature Salad", itemDescription: "Straight from the Original Shack. Sliced salami, grated mozzarella, marinated three-bean salad, hard-cooked eggs, sliced beets and fresh carrots, mushrooms, tomato and red onion on a bed of iceberg lettuce. ", defaultPrice: 5.95 , prices:["Small Salad":5.95, "Large Salad": 10.50], sauces:["Sonoma Italian","Thousand Island","Blue Cheese","Ranch","Tamari","Non-fat Raspberry Vinaigrette"], Extras: nil)
                
            case .CaesarSalad:
                
                return(itemName: "Caesar Salad", itemDescription: "Romaine lettuce, fresh Sonoma sourdough croutons and grated parmesan cheese tossed with Mary’s special Caesar dressing.", defaultPrice: 4.75 , prices:nil, sauces:nil, Extras: nil)
                
            case .SpinachSalad:
                
                return(itemName: "Spinach Salad", itemDescription: "Fresh spinach tossed with walnuts, raisins, cranberries, red onions and crumbled blue cheese.  We recommend our tamari  ", defaultPrice: 9.50 , prices:["Small Salad" : 9.50 , "Salad w/ Chicken" : 13.25], sauces:["Sonoma Italian","Thousand Island","Blue Cheese","Ranch","Tamari","Non-fat Raspberry Vinaigrette"] , Extras: nil)
                
            case .ItalianChoppedSalad:
                
                return(itemName: "Italian Chopped Salad", itemDescription: "Julienned salami and pepperoni, grated parmesan, sliced olives and garbanzo beans all tossed with shredded lettuce and our famous homemade Sonoma Italian dressing. Served with our fresh focaccia. ", defaultPrice: 9.75 , prices:nil, sauces:nil , Extras: nil)
                
            case .TheWedge:
                
                return(itemName: "The Wedge", itemDescription: "Crisp iceberg lettuce topped with blue cheese, bacon, marinated tomatoes and blue cheese dressing. Served with fresh focaccia.", defaultPrice: 6.25 , prices:["Half" : 6.25 , "Full" : 10.75], sauces:["Sonoma Italian","Thousand Island","Blue Cheese","Ranch","Tamari","Non-fat Raspberry Vinaigrette"], Extras:nil)
                
            case .CobbSalad:
                
                return(itemName: "Cobb Salad", itemDescription: "Chicken, bacon, blue cheese, egg, avocado and marinated tomatoes on chopped lettuce. ", defaultPrice: 12.75 , prices:nil, sauces:nil , Extras:nil)
                
            case .SoupandSaladCombo:
                
                return(itemName: "Soup & Salad Combo", itemDescription: "A bowl of our homemade soup of the day and your choice of a small Mary’s or Caesar salad served with fresh sourdough bread.  ", defaultPrice: 9.95 , prices:nil, sauces:nil , Extras: nil)
                
                
                
                
                //SANDWICHES
            case .MarysOriginalPizzaDoughHotDog:
                return(itemName: "Mary's Pizza Dough Hot Dog", itemDescription: "A favorite of kids big and small, an all-beef hot dog and mozzarella cheese wrapped in Mary’s homemade pizza dough, and baked to golden perfection. ", defaultPrice: 7.95 , prices:["One Hot Dog" : 7.95 , "Two Hot Dogs" : 9.95], sauces:["Fries", "Soup", "Salad"] , Extras: ["Avacado" : 1.25,   "Bacon" : 1.50, "Pesto" : 1.25] )
                
            case .MeatballSandwich:
                return(itemName: "Meatball Sandwich", itemDescription: "A knife-and-fork sandwich. Two giant meatballs smothered in Mary’s authentic old world meat sauce and topped with melted mozzarella cheese, served open-faced on a Sonoma sourdough roll.", defaultPrice: 11.95 , prices: nil, sauces:["Fries", "Soup", "Salad"] , Extras: ["Avacado" : 1.25,   "Bacon" : 1.50, "Pesto" : 1.25] )
                
            case .ChickenAndBeaconClub:
                return(itemName: "Chicken and Bacon Club", itemDescription: "Slices of chicken breast and thick-cut bacon are topped with lettuce, tomato, red onion, mayonnaise and Italian dressing on homemade focaccia.", defaultPrice: 10.75 , prices: nil, sauces:["Fries", "Soup", "Salad"] , Extras: ["Avacado" : 1.25, "Mozzarella" : 1.25 , "Pesto" : 1.25] )
                
            case .TurkeySupremo:
                return(itemName: "Turkey Supremo", itemDescription: "Sliced turkey, mozzarella and avocado topped with Italian dressing, mayonnaise, red onions, lettuce and tomato on our fresh focaccia. Available hot or cold. ", defaultPrice: 10.75 , prices: ["Hot" : 10.75 , "Cold" : 10.75], sauces:["Fries", "Soup", "Salad"] , Extras: [ "Pesto" : 1.25] )
                
            case .EggplantAndArtichokeSandwich:
                return(itemName: "Eggplant and Artichoke Sandwich", itemDescription: "Long-stemmed Italian artichoke hearts with crispy eggplant, roasted red peppers, fresh arugula, parmesan and spicy ailoi on our homemade focaccia.", defaultPrice: 10.75 , prices: nil, sauces:["Fries", "Soup", "Salad"] , Extras: [ "Pesto" : 1.25] )
                
            case .ItalianBLTA:
                return(itemName: "Italian BLTA", itemDescription: "The classic BLT made better with sliced avocado, melted provolone cheese, mayonaise and Mary’s Sonoma Italian dressing on focaccia bread. ", defaultPrice: 10.50 , prices: nil, sauces:["Fries", "Soup", "Salad"] , Extras: [ "Pesto" : 1.25] )
                
            case .GrilledChickenSandwich:
                return(itemName: "Chicken and Bacon Club", itemDescription: "A grilled chicken breast served on fresh focaccia with Mary’s Sonoma Italian dressing, mayonnaise, red onions, lettuce and tomato. ", defaultPrice: 10.75 , prices: nil, sauces:["Fries", "Soup", "Salad"] , Extras: ["Avacado" : 1.25, "Mozzarella" : 1.25 , "Pesto" : 1.25 , "Bacon" : 1.50] )
                
            case .MarysFamousHamburger:
                return(itemName: "Mary's Famous Hamburger", itemDescription: "A third-pound of Harris Ranch beef topped with Mary’s Thousand Island dressing, mayonnaise, grilled onions, lettuce and tomato. Served on a sourdough roll.", defaultPrice: 10.75 , prices: nil, sauces:["Fries", "Soup", "Salad"] , Extras: ["Avacado" : 1.25, "Mozzarella" : 1.25 , "Pesto" : 1.25 , "Bacon" : 1.50 , "Sauteed Mushrooms" : 1.25] )
                
            case .BuffaloChickenWrap:
                return(itemName: "Buffalo Chicken Wrap", itemDescription: "Crispy chicken, lettuce, red onion, tomatoes, and crunchy tortilla strips, tossed with spicy Buffalo sauce and a hint of blue cheese dressing.", defaultPrice: 8.95 , prices: nil, sauces:["Fries", "Soup", "Salad"] , Extras: ["Avacado" : 1.25, "Bacon" : 1.50] )
                
            case .ChickenCaesarWrap:
                return(itemName: "Chicken Caesar Wrap", itemDescription: "Mary’s signature Chicken Caesar salad with chopped romaine lettuce and a touch of ranch dressing mixed in. Croutons add a little crunch to every bite.", defaultPrice: 8.95 , prices: nil, sauces:["Fries", "Soup", "Salad"] , Extras: ["Avacado" : 1.25, "Bacon" : 1.50] )
                
            case .TurkeyWrap:
                return(itemName: "Turkey Wrap", itemDescription: "Spicy mayonnaise with thin slices of turkey wrapped around imported Italian olives, chopped lettuce dressed in balsamic, Italian sweet onion, sweet peppers with provolone cheese.", defaultPrice: 8.95 , prices: nil, sauces:["Fries", "Soup", "Salad"] , Extras: ["Avacado" : 1.25, "Bacon" : 1.50] )
                
                
                
                
                //Pastas and Entrees
            case .SpaghettiWithAGiantMeatball:
                return(itemName: "Spaghetti with a Giant Meatball", itemDescription: "Straight from Mary’s Original Shack. A giant homemade meatball on spaghetti or rigatoni tossed with our old world meat sauce.", defaultPrice: 14.25 , prices: nil, sauces:["Mary's Salad", "Caesar Salad", "Soup"] , Extras:nil )
                
            case .GnoochiWithAGiantMeatball:
                return(itemName: "Gnoochi with a Giant Meatball", itemDescription: "Mary’s favorite dish. Traditional Italian potato dumplings tossed with Mary’s meat sauce and topped with a giant homemade meatball.", defaultPrice: 15.95 , prices: nil, sauces:["Mary's Salad", "Caesar Salad", "Soup"] , Extras:nil )
                
            case .HeAndShe:
                return(itemName: "\"He & She\"", itemDescription: "Named after a beauty salon near the original Shack, two giant meatballs topped with our old world meat sauce and melted mozzarella.", defaultPrice: 10.95 , prices: nil, sauces:["Mary's Salad", "Caesar Salad", "Soup"] , Extras:nil )
                
            case .LinguineMarinara:
                return(itemName: "Linguine and Marinara", itemDescription: "Homemade marinara sauce from Mary’s own recipe, tossed with linguine. Simply perfect. ", defaultPrice: 12.65 , prices: ["Normal" : 12.65 , "Spicy" : 13.95 ], sauces:["Mary's Salad", "Caesar Salad", "Soup"] , Extras:nil )
                
            case .PennewithSausage:
                return(itemName: "Penne With Sausage", itemDescription: "Mild Italian sausage sautéed with mushrooms, red onions, garlic and Italian parsley, tossed with penne pasta and Mary’s marinara sauce.", defaultPrice: 12.65 , prices:nil, sauces:["Mary's Salad", "Caesar Salad", "Soup"] , Extras:nil )
                
            case .ProteinPlate:
                return(itemName: "Protein Plate", itemDescription: "Your choice of a grilled chicken breast or third-pound Harris Ranch hamburger patty topped with sautéed mushrooms. ", defaultPrice: 10.75 , prices: ["Chicken Breast" : 10.75 , "Hamburger Patty" : 10.75 ], sauces:["Mary's Salad", "Caesar Salad", "Soup"] , Extras:nil )
                
            case .ChickenFettuccineAlfredo:
                return(itemName: "Chicken Fettuccine Alfredo", itemDescription: "Creamy homemade Alfredo sauce tossed with sliced chicken breast, fresh mushrooms, red onions and fettuccine.", defaultPrice: 15.95 , prices:nil, sauces:["Mary's Salad", "Caesar Salad", "Soup"] , Extras:nil )
                
            case .ChickenPicatta:
                return(itemName: "Chicken Picatta", itemDescription: "Tender slices of chicken breast sautéed in a creamy lemon and caper sauce and served with fettuccine pasta.", defaultPrice: 15.25 , prices:nil, sauces:["Mary's Salad", "Caesar Salad", "Soup"] , Extras:nil )
                
            case .MeatRaviolli:
                return(itemName: "Meat Raviolli", itemDescription: "Choose your favorite. Hearty meat or cheese ravioli tossed with Mary’s old world meat sauce or homemade marinara sauce. ", defaultPrice: 15.25 , prices:["Old World Meat Sauce" : 15.25 , "Homemade Marinara Sauce" : 15.25], sauces:["Mary's Salad", "Caesar Salad", "Soup"] , Extras:nil )
                
            case .CheeseRaviolli:
                return(itemName: "Cheese Raviolli", itemDescription: "Choose your favorite. Hearty meat or cheese ravioli tossed with Mary’s old world meat sauce or homemade marinara sauce. ", defaultPrice: 15.25 , prices:["Old World Meat Sauce" : 15.25 , "Homemade Marinara Sauce" : 15.25], sauces:["Mary's Salad", "Caesar Salad", "Soup"] , Extras:nil )
                
            case .TortelliniallaMarys:
                return(itemName: "Tortellini alla Mary's", itemDescription: "Pasta filled with meat and tossed with your choice of creamy pesto or creamy marinara sauce.", defaultPrice: 15.95 , prices:["Pesto Sauce" : 15.95 , "Marinara Sauce" : 15.95], sauces:["Mary's Salad", "Caesar Salad", "Soup"] , Extras:nil )
                
            case .MarysClassicLasagna:
                return(itemName: "Mary's Classic Lasagna", itemDescription: "Sheets of pasta layered with Italian sausage, salami, spinach, mozzarella and  ricotta cheeses, topped with meat sauce and melted mozzarella.", defaultPrice: 14.95 , prices:nil, sauces:["Mary's Salad", "Caesar Salad", "Soup"] , Extras:nil )
                
            case .ChickenParmigiana:
                return(itemName: "Chicken Parmigiana", itemDescription: "Fresh chicken breast, lightly breaded and smothered in homemade marinara sauce and melted mozzarella cheese, served on a bed of creamy fettuccine Alfredo.", defaultPrice: 16.25 , prices:nil, sauces:["Mary's Salad", "Caesar Salad", "Soup"] , Extras:nil )
                
                
            case .EggplantParmigiana:
                return(itemName: "Eggplant Parmigiana", itemDescription: "Fresh eggplant slices, hand-breaded and layered with fresh marinara sauce, topped with mozzarella and parmesan cheeses and baked to perfection.", defaultPrice: 14.25 , prices:nil, sauces:["Mary's Salad", "Caesar Salad", "Soup"] , Extras:nil )
                
                //Desserts
                
            case .BlueberryCrumbleCheesecake:
                return(itemName: "Blueberry Crumble Cheesecake", itemDescription: "Creamy lemon cheesecake topped with blueberries and streusel.", defaultPrice: 5.50 , prices:nil, sauces: nil , Extras:nil )
                
            case .MarysOriginalMudPie:
                return(itemName: "Mary's Original Mud Pie", itemDescription: "Mary’s original recipe and our best seller!", defaultPrice: 5.95 , prices:nil, sauces: nil , Extras:nil )
                
            case .SaltedCaramelGelatoPie:
                return(itemName: "Salted Caramel Gelato Pie", itemDescription: "Salted caramel gelato in a cookie crust. ", defaultPrice: 5.95 , prices:nil, sauces: nil , Extras:nil )
                
            case .GiganticCookieSundae:
                return(itemName: "Gigantic Cookie Sundae", itemDescription: "Chocolate chip cookie served with a big scoop of vanilla ice cream  and topped with plenty of rich chocolate sauce.", defaultPrice: 5.50 , prices:["Cookie Sundae" : 5.50 , "Gigantic Cookie" : 3.95 ], sauces: nil , Extras:nil )
                
            case .MarysMessyBrownieSundae:
                return(itemName: "Mary's Messy Brownie Sundae", itemDescription: "A thick fudgy brownie served with a big scoop of vanilla ice cream and topped with plenty of rich chocolate sauce. ", defaultPrice: 5.50 , prices:["Brownie Sundae" : 5.50 , "Brownie" : 2.50 ], sauces: nil , Extras:nil )
                
            case .PineappleUpsideDownCake:
                return(itemName: "Pineapple Upside Down Cake", itemDescription: "Caramelized pineapple tops a circle of rich, buttery cake.  ", defaultPrice: 5.50 , prices:nil, sauces: nil , Extras:nil )
                
                
                //Med Menu
            case .BurrataAndArugula:
                return(itemName: "Burrata & Arugula", itemDescription: "A delight to your tastebuds. Creamy burrata cheese on top of a bed of arugula dressed in olive oil. Topped with our classic marinated Napoletana tomatoes with slices of toasted sourdough bread.", defaultPrice: 10.50 , prices:nil, sauces: nil , Extras:nil)
                
            case .TunaAndCouscous:
                return(itemName: "Tuna & Couscous", itemDescription: "Bed of romaine lettuce topped with couscous, garbanzo beans, green beans, corn, marinated onions, peas, heirloom tomatoes, tuna in light oil with a balsamic glaze and parmesan cheese. ", defaultPrice: 12.95 , prices:nil, sauces: nil , Extras:nil)
                
            case .RigatoniWithArtichokes:
                return(itemName: "Rigatoni With Artichokes", itemDescription: "A hearty dish of rigatoni tossed with imported Italian artichoke hearts, oven-roasted tomatoes, fresh arugula, chickpeas, parmesan and lemon juice. ", defaultPrice: 14.95 , prices:nil, sauces: ["Mary's Salad", "Caesar Salad", "Soup"] , Extras:nil)
                
                
            case .PestoPomodoro:
                return(itemName: "Pesto Pomodoro", itemDescription: "Linguine pasta tossed with basil pesto, oven-roasted tomatoes and feta cheese. A fresh take on an Italian classic. ", defaultPrice: 14.25 , prices:["Linguine" : 14.95, "Normal" : 14.25], sauces: ["Mary's Salad", "Caesar Salad", "Soup"] , Extras:nil)
                
            case .LinguineWithClams:
                return(itemName: "Linguine with Clams", itemDescription: "Baby clams and linguine tossed in a white wine sauce with garlic, red pepper flakes and olive oil. ", defaultPrice: 15.95 , prices:nil, sauces: ["Mary's Salad", "Caesar Salad", "Soup"] , Extras:nil)
                
            case .LemonShrimpLinguine:
                return(itemName: "Lemon Shrimp Linguine", itemDescription: "A fresh take on shrimp scampi with a creamy white wine and lemon sauce with fresh herbs on a bed of linguine.", defaultPrice: 15.95 , prices:nil, sauces: ["Mary's Salad", "Caesar Salad", "Soup"] , Extras:nil)
                
            case .PastaConTonno:
                return(itemName: "Pasta Con Tonno", itemDescription: "A Mary’s twist on a comfort food classic. Heirloom tomatoes, Italian olives, Italian sweet onions, sweet peppers, garlic, olive oil, crushed croutons, tuna in light oil and spaghetti noodles topped with parsley and parmesan. ", defaultPrice: 15.95 , prices:nil, sauces: ["Mary's Salad", "Caesar Salad", "Soup"] , Extras:nil)
                
            case .ItalianFarmersMarket:
                return(itemName: "Italian Farmers Market Pizza", itemDescription: "A surprise in every bite! Imported Italian olives, sweet peppers, sautéed spinach, Italian sweet onions, oven roasted tomatoes, and Italian eggplant on top of a blend of mozzarella and provolone, asiago and feta cheeses.", defaultPrice: 10.95 , prices:["Bambino" : 10.95 , "Medium" : 19.75], sauces: nil , Extras:nil)
                
                
                
                
                
                //Pizzas
            case .MarysCombinations:
                return(itemName: "Mary’s Combination", itemDescription: "Like Mary’s, it’s full of all that’s good: salami, pepperoni, cotto salami, mushrooms and Italian sausage with our legendary pizza sauce.", defaultPrice: 15.95 , prices:["Small" : 15.95 , "Medium" : 19.75, "Large" : 24.95 ], sauces: nil , Extras:nil)
                
            case .TotosCombo:
                return(itemName: "Toto's Combination", itemDescription: "All the good stuff and then some: salami, pepperoni, cotto salami, linguica, sausage, mushrooms, onions, bell peppers and olives.", defaultPrice: 18.25 , prices:["Small" : 18.25 , "Medium" : 23.50, "Large" : 28.75 ], sauces: nil , Extras:nil)
                
            case .HawaiianLuau:
                return(itemName: "Hawaiian Luau", itemDescription: "A sweet and savory classic with Canadian bacon, pineapple and our pizza sauce. Aloha!", defaultPrice: 9.95 , prices:["Bambino" : 9.95, "Small" : 14.75 , "Medium" : 18.95, "Large" : 24.50 ], sauces: nil , Extras:nil)
                
            case .BuffaloChicken:
                return(itemName: "Buffalo Chicken", itemDescription: "Chicken tossed in spicy Buffalo sauce, with lots of mozzarella cheese and fried onion strings.", defaultPrice: 10.95 , prices:["Bambino" : 10.95, "Small" : 15.25 , "Medium" : 19.25, "Large" : 24.50 ], sauces: nil , Extras:nil)
                
            case .TuscanGarlicChicken:
                return(itemName: "Tuscan Garlic Chicken", itemDescription: "It’s all about the sauce: roasted garlic sauce, grilled chicken, caramelized onions, and mozzarella, finished with a drizzle of olive oil.", defaultPrice: 10.95 , prices:["Bambino" : 10.95, "Small" : 15.25 , "Medium" : 19.25, "Large" : 24.50 ], sauces: nil , Extras:nil)
                
            case .PestoChicken:
                return(itemName: "Pesto Chicken", itemDescription: "Grilled pesto chicken, roasted tomatoes and three kinds of cheese: mozzarella, provolone and feta.", defaultPrice: 10.95 , prices:["Bambino" : 10.95, "Small" : 15.25 , "Medium" : 19.25, "Large" : 24.50 ], sauces: nil , Extras:nil)
                
            case .BBQChicken:
                return(itemName: "BBQ Chicken", itemDescription: "BBQ sauce topped with chicken, white cheddar, bacon, tri-color jalapeños, caramelized onions and crispy red onions on top.",  defaultPrice: 10.95 , prices:["Bambino" : 10.95, "Small" : 15.25 , "Medium" : 19.25, "Large" : 24.50 ], sauces: nil , Extras:nil)
                
            case .PizzaAlaSriracha:
                return(itemName: "Pizza a la Sriracha", itemDescription: "Bringing some spice to the Shack with Sriracha! Chicken with Sriracha sauce and ranch, white cheddar, pineapple, green onions and crispy red onions.",  defaultPrice: 10.95 , prices:["Bambino" : 10.95, "Small" : 15.25 , "Medium" : 19.25, "Large" : 24.50 ], sauces: nil , Extras:nil)
                
            case .ClassicCheeseCalzone:
                return(itemName: "Classic Cheese Calzone", itemDescription: "Homemade pizza dough filled with ricotta, parmesan and mozzarella cheeses, then topped with Mary’s homemade pizza sauce, fresh garlic and more cheese. Big enough to share!  ",  defaultPrice: 13.50 , prices:nil, sauces: nil , Extras:nil)
                
            case .VegetarianCalzone:
                return(itemName: "Vegetarian Calzone", itemDescription: "With a garden’s worth of fresh bell peppers, onions, olives, mushrooms and tomatoes.",  defaultPrice: 16.25 , prices:nil, sauces: nil , Extras:nil)
                
            case .MarysMeatCalzone:
                return(itemName: "Mary's Meat Calzone", itemDescription: "Our classic cheese calzone stuffed with salami, pepperoni and cotto salami.",  defaultPrice: 17.25 , prices:nil, sauces: nil , Extras:nil)
                
                
                
                
                
                
                
        
                
            default:
                return(itemName: "Mary's Original Pizza Dough Hot Dog", itemDescription: "A favorite of kids big and small, an all-beef hot dog and mozzarella cheese wrapped in Mary’s homemade pizza dough, and baked to golden perfection. ", defaultPrice: 7.95 , prices:["One Hot Dog" : 7.95 , "Two Hot Dogs" : 9.95], sauces:["Fries", "Soup", "Salad"] , Extras: ["Avacado" : 1.25, "Mozzarella" : 1.25 , "Bacon" : 1.50, "Pesto" : 1.25] )
                
                
                
                
            }
        }
        
    }
    class func sortDictionary(dict: [String: Double])-> (Sizes:[String] , Prices:[String])
    {
        
        var sizeArray = [String]()
        var pricesArray = [String]()
        _ = [String:String]()
        var arrayOfPrices =   Array(dict.values)
        
        
        var arrayOfItems =  [String]()
        let nsdict = dict as NSDictionary
        
        if nsdict.allKeys[0] as! String == "Chicken Breast" || nsdict.allKeys[0] as! String == "Hamburger Patty" {
            pricesArray = ["10.75","10.75"]
            sizeArray = ["Chicken Breast", "Hamburger Patty"]
            return(Sizes:sizeArray, Prices:pricesArray)
        }
        else if nsdict.allKeys[0] as! String == "Cold" || nsdict.allKeys[1] as! String == "Cold" {
            pricesArray = ["10.75","10.75"]
            sizeArray = ["Cold", "Hot"]
            return(Sizes:sizeArray, Prices:pricesArray)
        }
        else if nsdict.allKeys[0] as! String == "Old World Meat Sauce" || nsdict.allKeys[1] as! String == "Old World Meat Sauce" {
            pricesArray = ["15.25","15.25"]
            sizeArray = ["Meat Sauce", "Marinara"]
            return(Sizes:sizeArray, Prices:pricesArray)
        }
            
//          else if nsdict.allKeys[0] as! String == "w/ Feta" || nsdict.allKeys[1] as! String == "w/ Feta" || nsdict.allKeys[2] as! String == "w/ Feta"
//        {
//            sizeArray = ["Bruschetta","w/ Feta","w/ Mozzarella"]
//            pricesArray = ["9.50","10.50","10.50"]
//            
//            return(Sizes:sizeArray, Prices:pricesArray) // FIX BRUSCHETTA HERE
//        }
            
            
        else if nsdict.allKeys[0] as! String == "Pesto Sauce" || nsdict.allKeys[1] as! String == "Pesto Sauce" {
            pricesArray = ["15.95","15.95"]
            sizeArray = ["Pesto Sauce", "Marinara"]
            return(Sizes:sizeArray, Prices:pricesArray)
        }
       
           
        else {
            arrayOfPrices.sortInPlace {return $0 <= $1 }
            for price in arrayOfPrices {
                let array = nsdict.allKeysForObject(price)
                let item = array[0] as! String
                sizeArray.append(item)
            }
            
        }
        for var i = 0 ; i < arrayOfPrices.count ; i++
        {
            pricesArray.append("\(arrayOfPrices[i])")
         
       }
        return(Sizes:sizeArray, Prices:pricesArray)
//        
    }
  
    
}
