//
//  SortArray.swift
//  Mary's Pizza Shack
//
//  Created by Rich Blanchard on 8/10/15.
//  Copyright (c) 2015 Rich. All rights reserved.
//

import Foundation
protocol Sort {
    
   func sortDictionary(dict: [String: Double])-> (Sizes:[String] , Prices:[Double])
    
}
