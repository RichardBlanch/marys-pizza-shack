//
//  PhotosCollectionViewCell.swift
//  Mary's Pizza Shack
//
//  Created by Rich Blanchard on 8/14/15.
//  Copyright (c) 2015 Rich. All rights reserved.
//

import UIKit

class PhotosCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var instagramPhoto: UIImageView!
    
}
