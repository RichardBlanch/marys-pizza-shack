//
//  Restaurants.swift
//  Mary's Pizza Shack
//
//  Created by Rich Blanchard on 8/13/15.
//  Copyright (c) 2015 Rich. All rights reserved.
//

import Foundation
import UIKit
import CoreLocation

class Restaurants {
    class func sortArrayByDistanceFromCurrentLocation(distances:[Restaurant]) -> [Restaurant] {
        let newArray = distances.sort({ $0.distance < $1.distance })
        return newArray
    }
   
    class func getArray()->[Restaurant]
    {
       
    let array = [Restaurant(name: "Sonoma Plaza", phoneNumber: "(707) 938-8300", hours: "Sunday - Thursday 11am - 10pm Friday & Saturday, 11am - 11pm", coordinates: CLLocation(latitude: 38.293952, longitude: -122.4599511) , address:"8 W. Spain St.Sonoma, CA 95476"),
        
        Restaurant(name: "Anderson", phoneNumber: "(530) 378-1110", hours: "Sunday - Thursday 11am - 10pm Friday & Saturday, 11am - 11pm", coordinates: CLLocation(latitude: 40.435268, longitude: -122.288238) , address:"1901 Highway 273 Anderson, CA 96007"),
        
        Restaurant(name: "Boyes Hot Springs", phoneNumber: "(707) 938-3600", hours: "Sunday - Thursday 11am - 10pm Friday & Saturday, 11am - 11pm" , coordinates: CLLocation(latitude: 38.3105725, longitude: -122.487763) , address:"18636 Sonoma Highway Boyes Hot Springs, CA 95476"),
        
        Restaurant(name: "Cloverdale", phoneNumber: "(707) 894-8977", hours: "Sunday - Thursday 11am - 9:30pm Friday & Saturday, 11am - 10pm", coordinates: CLLocation(latitude: 38.783422, longitude: -123.014474) , address:"1143 South Cloverdale Boulevard, Cloverdale, CA 95425"),
        
        Restaurant(name: "Dixon", phoneNumber: "(707) 693-0300", hours: "Sunday - Thursday 11am - 10pm Friday & Saturday, 11am - 11pm", coordinates: CLLocation(latitude: 38.457013, longitude: -121.844007) , address:"1460 Ary Lane Dixon, CA 95620"),
        
        Restaurant(name: "Fairfield", phoneNumber: "(707) 422-2700", hours: "Sunday - Thursday 11am - 10pm Friday & Saturday, 11am - 10pm", coordinates: CLLocation(latitude: 38.3065325, longitude: -122.2827468) , address:"1500 Oliver Rd. Fairfield, CA 94534"),
        
        Restaurant(name: "Napa", phoneNumber: "(707) 257-3300", hours: "Sunday - Thursday 11am - 10pm Friday & Saturday, 11am - 10pm", coordinates: CLLocation(latitude: 38.2814197, longitude: -122.2908841) , address:"3085 Jefferson St. Napa, CA 94558-4921"),
        
        Restaurant(name: "Novato", phoneNumber: "(415) 897-6266", hours: "Sunday - Thursday 11am - 10pm Friday & Saturday, 11am - 10pmv", coordinates: CLLocation(latitude: 38.1227482, longitude: -122.6052409) , address:"121 San Marin Dr. Novato, CA 94945"),
        
        Restaurant(name: "Petaluma East", phoneNumber: "(707) 765-1959", hours: "Sunday - Thursday 11am - 10pm Friday & Saturday, 11am - Midnight", coordinates: CLLocation(latitude: 38.247026, longitude: -122.621840) , address:"423 N. McDowell Blvd. Petaluma, CA 94952"),
        
        Restaurant(name: "Petaluma West", phoneNumber: "(707) 778-7200", hours: "Sunday - Thursday 11am - 10pm Friday & Saturday, 11am - 11pm", coordinates: CLLocation(latitude: 38.237984, longitude: -122.636292) , address:"359 East Washington St. Petaluma, CA 94952"),
        
        Restaurant(name: "Redding", phoneNumber: "(530) 247-1110", hours: "Sunday - Thursday 11am - 9pm Friday & Saturday, 11am - 10pm", coordinates: CLLocation(latitude: 40.5868939, longitude: -122.2367987) , address:"1177 Shasta St. Redding, CA 96001"),
        
        Restaurant(name: "Rohnert Park", phoneNumber: "(707) 585-3500", hours: "Sunday - Thursday 11am - 10:30pm Friday & Saturday, 11am - 11pm", coordinates: CLLocation(latitude: 38.3636453, longitude: -122.711633) , address:"101 Golf Course Dr. Rohnert Park, CA 94928"),
        
        Restaurant(name: "Roseville", phoneNumber: "(916) 780-7600", hours: "Sunday - Thursday 11am - 10pm Friday & Saturday, 11am - 11pm", coordinates: CLLocation(latitude: 38.79127, longitude: -121.278234) , address:"711 Pleasant Grove Blvd. Roseville/Rocklin , CA 95678"),
        
        Restaurant(name: "Santa Rosa Downtown", phoneNumber: "(707) 571-1959", hours: "Sunday - Thursday 11am - 11pm Friday & Saturday, 11am - midnight", coordinates: CLLocation(latitude: 38.4410032, longitude: -122.7157996) , address:"615 Fourth St. Santa Rosa, CA 95404"),
        
        Restaurant(name: "Santa Rosa East", phoneNumber: "(707) 538-1888", hours: "Sunday - Thursday, 11am - 10:30 pm Friday and Saturday, 11am - 11pm", coordinates: CLLocation(latitude: 38.453856, longitude: -122.6737776) , address:"535 Summerfield Rd. Santa Rosa, CA 95405"),
        
        Restaurant(name: "Santa Rosa West", phoneNumber: "(707) 573-1100", hours: "Sunday - Thursday 11am - 10pm Friday & Saturday, 11am - 11pm", coordinates: CLLocation(latitude: 38.4303611, longitude: -122.6954248) , address:"3084 Marlow Rd. Santa Rosa, CA 95403"),
        
        Restaurant(name: "Sebastopol", phoneNumber: "(707) 829-5800", hours: "Sunday - Thursday 11am - 10pm Friday & Saturday, 11am - 11pm", coordinates: CLLocation(latitude: 38.3973792, longitude: -122.829602) , address:"790 Gravenstein Highway North Sebastopol , CA 95472"),
        
        Restaurant(name: "Vacaville", phoneNumber: "(707) 446-7100", hours: "Sunday - Thursday 11am - 10pm Friday & Saturday, 11am - 10pm", coordinates: CLLocation(latitude: 38.353496, longitude: -121.986081) , address:"505 Davis St. Vacaville, CA 95688"),
        
        Restaurant(name: "Walnut Creek", phoneNumber: "(925) 938-4800", hours: "Sunday - Thursday 11am - 10pm Friday & Saturday, 11am - 10pm", coordinates: CLLocation(latitude: 37.930999, longitude: -122.014659) , address:"2246 Oak Grove Rd. Walnut Creek, CA 94598"),
        
        Restaurant(name: "Windsor", phoneNumber: "(707) 836-0900", hours: "Sunday - Thursday 11am - 10pm Friday & Saturday, 11am - 11pm", coordinates: CLLocation(latitude: 38.5373544, longitude: -122.8084339) , address:"9010 Brooks Rd. Windsor, CA 95492")
        
    ]
    return array
    }
}






