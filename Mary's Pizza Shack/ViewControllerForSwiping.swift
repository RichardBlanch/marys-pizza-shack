//
//  ViewControllerForSwiping.swift
//  Mary's Pizza Shack
//
//  Created by Rich Blanchard on 8/11/15.
//  Copyright (c) 2015 Rich. All rights reserved.
//

import UIKit

class ViewControllerForSwiping: UIViewController{
    
    
     let defaults = NSUserDefaults.standardUserDefaults()
  
    @IBOutlet weak var animatedLabel: MTAnimatedLabel!
    override func viewDidDisappear(animated: Bool) {
        animatedLabel.stopAnimating()
    }
  
   
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
       
       self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        animatedLabel.startAnimating()
               // print(self.defaults.objectForKey("FirstName"),self.defaults.objectForKey("LastName"),self.defaults.objectForKey("CreditCardNumber"),self.defaults.objectForKey("ExpirationDate"),self.defaults.objectForKey("CVC"))
       
        if(self.defaults.valueForKey("PaymentOption") == nil) {
        presentAlertForPaymentOption()
        }
        else {
            print("Payment option = \(self.defaults.valueForKey("PaymentOption"))")
            if(self.defaults.valueForKey("PaymentOption") === "Venmo") {
                Venmo.sharedInstance().defaultTransactionMethod = VENTransactionMethod.AppSwitch
                Venmo.sharedInstance().requestPermissions([VENPermissionMakePayments,VENPermissionAccessProfile], withCompletionHandler: { (suceess, error) -> Void in
                    if(suceess) {
                        print("PERMISSIONS WORKED")
                    }
                    else {
                        print("No dice")
                    }
                })
            }
        }
    }
    
    func presentAlertForPaymentOption() {
       
       
        let alert = UIAlertController(title: "Select Payment Option", message: "", preferredStyle: UIAlertControllerStyle.Alert)
        let manual = UIAlertAction(title: "Manual", style: UIAlertActionStyle.Default) { (action) -> Void in
            self.defaults.setObject("Manual", forKey: "PaymentOption")
            self.presentAlertForManualCreditCard()
        }
        let venmo = UIAlertAction(title: "Venmo", style: UIAlertActionStyle.Default) { (action) -> Void in
            self.defaults.setObject("Venmo", forKey: "PaymentOption")
            Venmo.sharedInstance().defaultTransactionMethod = VENTransactionMethod.AppSwitch
            Venmo.sharedInstance().requestPermissions([VENPermissionMakePayments,VENPermissionAccessProfile], withCompletionHandler: { (suceess, error) -> Void in
                if(suceess) {
                    print("PERMISSIONS WORKED")
                }
                else {
                    print("No dice")
                }
            })
            
        }
        let applePay = UIAlertAction(title: "Apple Pay", style: UIAlertActionStyle.Default) { (action) -> Void in
            self.defaults.setObject("ApplePay", forKey: "PaymentOption")
        }
        alert.addAction(manual)
        alert.addAction(venmo)
        alert.addAction(applePay)
        presentViewController(alert, animated: true) { () -> Void in
            
        }
        
    }
    func presentAlertForManualCreditCard() {
        
       
        let alertController = UIAlertController(title: "Enter Credit Card Info", message: "", preferredStyle: UIAlertControllerStyle.Alert)
        
        
        
        alertController.addTextFieldWithConfigurationHandler { (textField) -> Void in
            textField.placeholder = "Enter First Name"
        }
        alertController.addTextFieldWithConfigurationHandler { (textField) -> Void in
            textField.placeholder = "Enter Last Name"
        }
         alertController.addTextFieldWithConfigurationHandler { (textField) -> Void in
            textField.placeholder = "Enter Credit Card Number"
        }
             alertController.addTextFieldWithConfigurationHandler { (textField) -> Void in
                textField.placeholder = "Enter Expiration Date MM/YYYY"
        }
             alertController.addTextFieldWithConfigurationHandler { (textField) -> Void in
                textField.placeholder = "Enter CVC"
        }
        
        alertController.addAction(UIAlertAction(title: "Close", style: UIAlertActionStyle.Cancel, handler:nil))
        alertController.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default, handler:{ (UIAlertAction)in
        self.defaults.setObject(alertController.textFields?.first?.text, forKey: "FirstName")
        self.defaults.setObject(alertController.textFields![1].text, forKey: "LastName")
        self.defaults.setObject(alertController.textFields![2].text, forKey: "CreditCardNumber")
        self.defaults.setObject(alertController.textFields![3].text, forKey: "ExpirationDate")
        self.defaults.setObject(alertController.textFields![4].text, forKey: "CVC")
            
            
            
        }))
       
        presentViewController(alertController, animated: true) { () -> Void in
            
        }
        
        
    }
    

}

