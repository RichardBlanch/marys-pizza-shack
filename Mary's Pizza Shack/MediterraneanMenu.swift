//
//  MediterraneanMenu.swift
//  Mary's Pizza Shack
//
//  Created by Rich Blanchard on 8/8/15.
//  Copyright (c) 2015 Rich. All rights reserved.
//

import Foundation
import UIKit

class Med : MenuItem
{
    class func getItemsAndImages() -> (array: [Med], image:UIImage)
    {
        let items = [Med(item: .BurrataAndArugula), Med(item: .EggplantAndArtichokeSandwich), Med(item: .TunaAndCouscous), Med(item: .RigatoniWithArtichokes), Med(item: .PestoPomodoro), Med(item: .LinguineWithClams), Med(item: .LemonShrimpLinguine), Med(item: .PastaConTonno), Med(item: .ItalianFarmersMarket)]
        
        let image = UIImage(named: "header-mediterranean_2")
        
        
        return (array: items, image: image!)
        
    }
    
}