//
//  menuCustomView.swift
//  Mary's Pizza Shack
//
//  Created by Rich Blanchard on 8/6/15.
//  Copyright (c) 2015 Rich. All rights reserved.
//

import UIKit
protocol SizeForScrollView
{
    func giveSize(point:CGPoint)
    func didClickButton(appetizer:MenuItem)
}

class menuCustomView: UIView, changedPage  {
    
    //MARK: Properties
    
    var appetizers:[MenuItem]!
    var lastSpot: CGPoint!
    var imageView:UIImageView!
    var offsetFromImageView:Int!
    var imageForItem:UIImage!
    var pageControl : UIPageControl!

   
    
    
    var delegate:SizeForScrollView?
    
    var count:Int! {
        didSet {
            setNeedsLayout()
        }
    }
    var itemButtons = [UIButton]()
    var textViews = [UITextView]()
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
   
    //MARK: Delegates
    func didChangePage() {
       
        self.setNeedsDisplay()
        
    }
    
    

    
    //MARK: Layout Subviews
    override func layoutSubviews() {
        self.frame.origin.y = superview!.frame.origin.y
       
        
       
        
        
        if(count != nil)
        {
        setUpImageViewForMenuItemPicture()
        setUpButtonsAndTextViews()
        
        
        let buttonSize = 10
        var buttonFrame = CGRect(x: 0, y: 0, width: screenRect.width - 20, height: 30)
        var index = 0
        
        let textViewSize = 100
        var textViewFrame = CGRectMake(0, buttonFrame.origin.y+25, screenRect.width - 20, 400)
        
        
        
        for var i = 0; i < itemButtons.count; i++
     {
            buttonFrame.origin.y = CGFloat(i * (buttonSize + 100) + offsetFromImageView)
            textViewFrame.origin.y = buttonFrame.origin.y + 25
        
            itemButtons[i].frame = buttonFrame
            textViews[i].frame = textViewFrame
        
        if( i == (itemButtons.count-1))
        {
            lastSpot = CGPointMake(frame.size.width, textViews[i].frame.origin.y + 175)
            delegate?.giveSize(lastSpot)
            break
        }
      
        
    }
        }
        
    }
    func menuItemSelected(button: UIButton) {
        var segueAppetizer:MenuItem!
        if let i = itemButtons.indexOf(button)
        {
            
        let segueAppetizer = appetizers[i]
        delegate?.didClickButton(segueAppetizer)
        }
    }
    
    
    //MARK: Setting up imageViews textViews
    func setUpButtonsAndTextViews() {
        backgroundColor = UIColor(patternImage: UIImage(named: "yellow")!)
        pageControl = UIPageControl()
        pageControl.frame = CGRectMake(110,-15,100,100);
        pageControl.tintColor = UIColor.marysRed()
        pageControl.pageIndicatorTintColor = UIColor.marysGreen()
        pageControl.numberOfPages = 7;
        pageControl.currentPage = 0;
        self.addSubview(pageControl)
       
        itemButtons = []
        if count != nil {
           
           
        for var i = 0; i < count; i++
        {
           
            
            let appetizerItem = appetizers[i]
            
            
            
            let button = UIButton()
            button.setTitle(appetizerItem.item.getItem().itemName, forState: .Normal)
            if(i == 0) {
               pageControl.currentPage = getPageControlIndex(button)
            }
            button.addTarget(self, action: "menuItemSelected:", forControlEvents: .TouchDown)
            button.contentHorizontalAlignment = .Left
            button.setTitleColor(UIColor(red: 182.0/255.0, green: 37.0/255.0, blue: 37/255.0, alpha: 1.0), forState: .Normal)
            button.titleLabel?.font = UIFont(name: "Cabin-Bold", size: 22.0)
            button.titleEdgeInsets = UIEdgeInsetsMake(0, 10, 0, 0)
            button.titleColorForState(.Normal)

            itemButtons += [button]
            addSubview(button)
            
            let textViewForDesciption = UITextView()
            textViewForDesciption.text = appetizerItem.item.getItem().itemDescription
            textViewForDesciption.selectable = false
            textViewForDesciption.editable = false
            textViewForDesciption.backgroundColor = UIColor.clearColor()
            textViewForDesciption.font = UIFont(name: "Cabin-Regular", size: 12.0)
            textViewForDesciption.textContainer.lineBreakMode = NSLineBreakMode.ByWordWrapping
            textViews += [textViewForDesciption]
            addSubview(textViewForDesciption)
            
           
            
        }
           
            
        }
    }
    func setUpImageViewForMenuItemPicture() {
        if let image = imageForItem
        {
        
        imageView = UIImageView(frame: CGRectMake(screenRect.width / 2 - (250/2), 55, 250, 50))
        imageView.image = image
        addSubview(imageView)
        offsetFromImageView = Int(imageView.frame.height + 75)
            
        }
    }
}
func getPageControlIndex(button: UIButton)->Int {
    let stringIndex = button.titleLabel!.text
    switch(stringIndex!) {
    case "Bread Sticks":
        return 0
    case "Homemade Soup of the Day":
        return 1
    case "Buffalo Chicken Wrap":
        return 2
    case "Spaghetti with a Giant Meatball":
        return 3
    case "Blueberry Crumble Cheesecake":
        return 4
    case "Burrata & Arugula":
        return 5
   
    default: return 6
    }
    
    
}
