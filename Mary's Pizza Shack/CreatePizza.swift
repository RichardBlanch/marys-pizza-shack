//
//  CreatePizza.swift
//  Mary's Pizza Shack
//
//  Created by Rich Blanchard on 8/16/15.
//  Copyright (c) 2015 Rich. All rights reserved.
//

import Foundation

class Toppings {
    var on : Bool!
    class func getTradtionalToppings()->Set<PizzaWithTopping>
    {
    let traditionalToppings: Set = [PizzaWithTopping(on: false, topping: "Anchovy & Garlic"), PizzaWithTopping(on: false, topping: "Bell Pepper"), PizzaWithTopping(on: false, topping: "Black Olive"), PizzaWithTopping(on: false, topping: "Canadian Bacon"), PizzaWithTopping(on: false, topping: "Carmelized Onions"), PizzaWithTopping(on: false, topping: "Cotto Salami"), PizzaWithTopping(on: false, topping: "Crumbled Blue Cheese"), PizzaWithTopping(on: false, topping: "Salami"), PizzaWithTopping(on: false, topping: "Extra Cheese"), PizzaWithTopping(on: false, topping: "Fresh Garlic"), PizzaWithTopping(on: false, topping: "Fresh Tomato"), PizzaWithTopping(on: false, topping: "Italian Sausage"), PizzaWithTopping(on: false, topping: "Linguica"), PizzaWithTopping(on: false, topping: "Meatball"), PizzaWithTopping(on: false, topping: "Mushroom"), PizzaWithTopping(on: false, topping: "Mushroom"), PizzaWithTopping(on: false, topping: "Pepporoni"), PizzaWithTopping(on: false, topping: "Pineapple")]
    return traditionalToppings
    }
    
    
    class func getSpecialtyToppings()->Set<PizzaWithTopping>
    {
    let SpecialtyTopping : Set  = [PizzaWithTopping(on: false, topping: "Artichoke Hearts"), PizzaWithTopping(on: false, topping: "Feta Cheese"), PizzaWithTopping(on: false, topping: "Fresh Spinach"), PizzaWithTopping(on: false, topping: "Grilled Chicken"), PizzaWithTopping(on: false, topping: "Tri-Color Jalopenos"), PizzaWithTopping(on: false, topping: "Pepper Bacon"),PizzaWithTopping(on: false, topping:  "Pesto Sauce"), PizzaWithTopping(on: false, topping: "Ranch Dressing"), PizzaWithTopping(on: false, topping: "Oven Roasted Tomatoes"), PizzaWithTopping(on: false, topping: "Whole Baby Clams"), PizzaWithTopping(on: false, topping: "Italian Olives"), PizzaWithTopping(on: false, topping: "Italian Eggplan")]
        
        return SpecialtyTopping
    }
    
    
}
