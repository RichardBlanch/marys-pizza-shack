//
//  PickSaucesViewController.swift
//  Mary's Pizza Shack
//
//  Created by Rich Blanchard on 8/10/15.
//  Copyright (c) 2015 Rich. All rights reserved.
//

import UIKit

class PickSaucesViewController: UIViewController , UIPickerViewDataSource, UIPickerViewDelegate
{
   
    var item: MenuItem!
    
    let blurredView = UIVisualEffectView(effect: UIBlurEffect(style: .Dark))
    
    var delegate: addSauces?
    var stringFromPickerView: String?
    
    init(fromView:UIView)
    {
        super.init(nibName: nil, bundle: nil)
        self.modalPresentationStyle = .OverCurrentContext
        self.modalTransitionStyle = .CoverVertical
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpBlurAndPickerView()
        setUpButtons()
        
    }
    
    
    
    
    
    
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return item.item.getItem().sauces!.count
        
    }
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 1
    }
    
    
    func pickerView(pickerView: UIPickerView, attributedTitleForRow row: Int, forComponent component: Int) -> NSAttributedString? {
        let title = item.item.getItem().sauces![row]
        
        let attributedString = NSAttributedString(string: title, attributes: [NSForegroundColorAttributeName:UIColor.whiteColor()])
        
        return attributedString;
    }
    
    
    
    
    
    func setUpBlurAndPickerView(){
        blurredView.frame = self.view.frame
        self.view.addSubview(blurredView)
        let  pickerView = UIPickerView(frame: CGRectMake(self.view.center.x - 200, self.view.center.y - 100, 400, 200))
        pickerView.delegate = self
        pickerView.dataSource = self
        self.view.addSubview(pickerView)
    }
    func setUpButtons() {
        //Cancel Button
        let cancelButton = UIButton(frame: CGRectMake(15, 20, 30, 30))
        cancelButton.setTitle("x", forState: .Normal)
        cancelButton.addTarget(self, action: "dismiss", forControlEvents: .TouchUpInside)
        let font = UIFont.systemFontOfSize(40)
        cancelButton.setTitleColor(UIColor.whiteColor(), forState: .Normal)
        view.addSubview(cancelButton)
        
        let doneButton = UIButton(frame: CGRectMake(self.view.frame.width-90, 20, 100, 30))
        doneButton.setTitle("Done", forState: .Normal)
        doneButton.addTarget(self, action: "addSelections", forControlEvents: .TouchUpInside)
        view.addSubview(doneButton)
        
    }
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        stringFromPickerView = item.item.getItem().sauces![row]
    }
    func dismiss() {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    func addSelections() {
        if let sauceToAdd = stringFromPickerView {
        delegate?.addSauces(sauceToAdd)
         self.dismissViewControllerAnimated(true, completion: nil)
    }
        else {
            stringFromPickerView = item.item.getItem().sauces![0]
            delegate?.addSauces(stringFromPickerView!)
            self.dismissViewControllerAnimated(true, completion: nil)
        }
    
    }
   override func  prefersStatusBarHidden() -> Bool {
    return true
    }
   
    
}
