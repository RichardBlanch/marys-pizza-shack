//
//  ViewController.swift
//  Mary's Pizza Shack
//
//  Created by Rich Blanchard on 8/6/15.
//  Copyright (c) 2015 Rich. All rights reserved.
//

import UIKit
protocol changedPage
{
    func didChangePage()
}

class ViewController: UIViewController,UIPageViewControllerDataSource,UIPageViewControllerDelegate {
    var pageViewController: UIPageViewController!
    
    
     var menuItems: [[MenuItem]]!
    
     var menuPictures: [UIImage]!
    
     var delegate:changedPage?
    
    @IBOutlet weak var customView: menuCustomView!
    
    var pageIndex: Int!
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBarHidden = true
        self.tabBarController?.tabBar.hidden
       
      
       
    }
    
    
    override func viewDidLoad() {
        setUpMenuAndGetImages()
        
        self.delegate = customView
        
        
      

        
        
        //MARK: Page Content ViewControlelr Stuff
        self.pageViewController = self.storyboard?.instantiateViewControllerWithIdentifier("PageViewController") as! UIPageViewController
        
       // self.pageViewController.spineLocation = UIPageViewControllerSpineLocation.None
        
        self.pageViewController.dataSource = self
        
        
        var startVC = self.viewControllerAtIndex(0) as ContentViewController
        
        var viewControllers = NSArray(object: startVC)
        
        
        
        self.pageViewController.setViewControllers(viewControllers as! [UIViewController], direction: .Forward, animated: true, completion: nil)
        
        
        
        self.pageViewController.view.frame = CGRectMake(0, 0, self.view.frame.width, self.view.frame.size.height)
        
        
        
        self.addChildViewController(self.pageViewController)
        
        self.view.addSubview(self.pageViewController.view)
        
        self.pageViewController.didMoveToParentViewController(self)
        
    
    
    
    
    
    
    }
    func viewControllerAtIndex(index:Int)->ContentViewController
    {
        if(self.menuItems.count == 0) || (index >= menuItems.count)
        {
            return ContentViewController()
        }
        let vc: ContentViewController = self.storyboard?.instantiateViewControllerWithIdentifier("ContentViewController") as! ContentViewController
        vc.pageIndex = index
        vc.appetizers = self.menuItems[index]
        vc.menuItemImage = menuPictures[index]
        vc.count = self.menuItems.count
       
       
        return vc
    }
    
    
    func pageViewController(pageViewController: UIPageViewController, viewControllerBeforeViewController viewController: UIViewController) -> UIViewController? {
        let vc = viewController as! ContentViewController
        var index = vc.pageIndex as Int
        
        if(index == 0 || index == NSNotFound)
        {
            return nil
        }
    tabBarController?.tabBar.hidden = false
    index--
    delegate?.didChangePage()
    return self.viewControllerAtIndex(index)
    }
    func pageViewController(pageViewController: UIPageViewController, viewControllerAfterViewController viewController: UIViewController) -> UIViewController? {
        let vc = viewController as! ContentViewController
        var index = vc.pageIndex as Int
        if(index == NSNotFound)
        {
            return nil
        }
         delegate?.didChangePage()
        index++
        tabBarController?.tabBar.hidden = false
        if(index == self.menuItems.count)
        {
            return nil
        }
        return self.viewControllerAtIndex(index)
    }
    func presentationCountForPageViewController(pageViewController: UIPageViewController) -> Int {
        return self.menuItems.count
    }
    func presentationIndexForPageViewController(pageViewController: UIPageViewController) -> Int {
        return 0
    }
    

    

    

    @IBAction func restartAction(sender: AnyObject) {
    }
    override func prefersStatusBarHidden() -> Bool {
        return true
    }
    func setUpMenuAndGetImages() {
    self.menuItems = [Appetizer.getItemsAndImage().array, Salad.getItemsAndImage().array, Sandwiches.getItemsAndImages().array, PastasAndEntrees.getItemsAndImages().array, Deserts.getItemsAndImage().array, Med.getItemsAndImages().array,Pizza.getItemsAndImages().array]
    
    self.menuPictures = [Appetizer.getItemsAndImage().image, Salad.getItemsAndImage().image, Sandwiches.getItemsAndImages().image, PastasAndEntrees.getItemsAndImages().image, Deserts.getItemsAndImage().image, Med.getItemsAndImages().image, Pizza.getItemsAndImages().image]
    }

}
