//
//  InstagamPhotosViewController.swift
//  Mary's Pizza Shack
//
//  Created by Rich Blanchard on 8/14/15.
//  Copyright (c) 2015 Rich. All rights reserved.
//

import UIKit

class InstagamPhotosViewController: UIViewController,UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout  {
    @IBOutlet weak var collectionView: UICollectionView!
    var count = 0
    let activityView = DGActivityIndicatorView(type:  DGActivityIndicatorAnimationType.RotatingSquares)
    
   
    override func viewWillAppear(animated: Bool) {
        
        super.viewWillAppear(true)
        self.collectionView.reloadData()
        self.tabBarController?.tabBar.hidden = true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        collectionView.backgroundColor = UIColor.yellowGifBackground()
        collectionView.scrollEnabled = true
        self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        fetchPicture()
       
        activityView.tintColor = UIColor.marysGreen()
        activityView.frame = CGRectMake((screenRect.width / 2) - 50, (screenRect.height / 2) - 50, 100, 100)
        activityView.startAnimating()
        view.addSubview(activityView)
        
    }
    
    
    func fetchPicture() {
        if count < 1
        {
            
            
            
            
            let config = NSURLSessionConfiguration.defaultSessionConfiguration()
            
            let request = NSMutableURLRequest(URL: NSURL(string: "https://api.instagram.com/v1/tags/gozar/media/recent?access_token=65f4e67b0a3a4045bdf290e7c88f562c")!)
            let session = NSURLSession(configuration: config)
            let task = session.dataTaskWithRequest(request, completionHandler: { (data : NSData?, response : NSURLResponse?, error : NSError?) -> Void in
                if (error != nil) {
                    print("Error: \(error!.localizedDescription)", terminator: "")
                }
                print(data)
               
            
            
                if let json: NSDictionary = (try? NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions.MutableContainers)) as? NSDictionary {
                 
                   
                    if let items = json["data"] as? NSArray {
                        
                        for image in items {
                            let imagesDictonary = image["images"] as! NSDictionary
                            let standardResDict = imagesDictonary["standard_resolution"] as! NSDictionary
                            if let url = standardResDict["url"] as? String
                            {
                                
                                
                                if !imageCacher.shardCacher.urls.contains(url) {
                                    
                                    if(url ==  "https://scontent.cdninstagram.com/hphotos-xpa1/t51.2885-15/s612x612/e35/10518259_112950655718324_171750784_n.jpg")
                                    {
                                        continue
                                    }
                                    
                                    imageCacher.shardCacher.urls.insert(url)
                                    if let URL = NSURL(string: url)
                                    {
                                        var dataOptional :NSData?
                                        dataOptional = NSData(data: NSData(contentsOfURL:URL)!)
                                        if let data = dataOptional
                                        {
                                            var image = UIImage(data: data)
                                            imageCacher.shardCacher.cache.setObject(data, forKey: url)
                                            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                                                self.collectionView.reloadData()
                                            })
                                        }
                                    }
                                    
                                }
                            }
                            
                        }
                        
                        
                        
                    }
                    
                    
                }
            
            
            })
            task.resume()
            count++
        }
    
    }

            
}
