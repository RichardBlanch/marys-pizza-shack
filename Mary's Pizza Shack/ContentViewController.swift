//
//  ContentViewController.swift
//  Mary's Pizza Shack
//
//  Created by Rich Blanchard on 8/6/15.
//  Copyright (c) 2015 Rich. All rights reserved.
//

import UIKit


class ContentViewController: UIViewController, SizeForScrollView
{
    @IBOutlet weak var scrollView: UIScrollView!
    var pageIndex: Int!
    
    var appetizers: [MenuItem]!
    
    var count: Int!
    
    var menuItemforSegue: MenuItem!
    
    var menuItemImage: UIImage!
    
   
    
    
    
    @IBOutlet weak var menuView: menuCustomView!

 
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        tabBarController?.tabBar.hidden = false
         navigationController?.navigationBarHidden = true
        menuView.delegate = self
        menuView.backgroundColor = UIColor.blackColor()
        menuView.appetizers = appetizers
        menuView.imageForItem = menuItemImage
        count = appetizers.count
        menuView.count = count
       
        
        self.view.backgroundColor = UIColor.yellowGifBackground()
        
               
    }
    override func prefersStatusBarHidden() -> Bool {
        return true
    }
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "detailView" {
            if let menuItemThatWasClicked = menuItemforSegue {
                let vc = segue.destinationViewController as! DetailMenuController
               vc.menuItem = menuItemThatWasClicked
            }
            
        }
        
    }
    
    //MARK: SELF MADE DELEGATE MERhods
    func giveSize(point: CGPoint) {
        scrollView.scrollEnabled = true
        
        
        scrollView.contentSize = CGSizeMake(screenRect.width, point.y)
    }
    func didClickButton(appetizer: MenuItem) {
        menuItemforSegue = appetizer
      
         self.performSegueWithIdentifier("detailView", sender: self)
    }

}

