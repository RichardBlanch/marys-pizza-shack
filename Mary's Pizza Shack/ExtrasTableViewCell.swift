//
//  ExtrasTableViewCell.swift
//  Mary's Pizza Shack
//
//  Created by Rich Blanchard on 8/11/15.
//  Copyright (c) 2015 Rich. All rights reserved.
//

import UIKit
protocol giveState {
    func giveAdditionalPrice(totalPrice: Double)
    func giveAdditionalExtras(extras : String)
    func removeAdditionalPrice(totalPrice:Double)
    func removeAdditionalExtras(extras : String)
    
}

class ExtrasTableViewCell: UITableViewCell{
   

    @IBOutlet weak var extraLabel: UILabel!
    @IBOutlet weak var `switch`: UISwitch!
    var delegate: giveState?

    var Extras : [String]!
    var count:Int!
    
    
    var menuItem:MenuItem! {
        didSet {
            
            if let extras  = menuItem.item.getItem().Extras
            {
                
                let arrayForExtras = Array(extras.keys)
                
                let value:String = arrayForExtras[count]
                extraLabel.text = value
            }
            
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib() // Initialization code
       
        
       
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

    @IBAction func switchToggled(sender: UISwitch) {
        let tableViewCell = sender.superview?.superview as! UITableViewCell
        let tableView =  superview?.superview as! UITableView
        let indexPath = tableView.indexPathForCell(tableViewCell)
       let arrayOfFuckSwift2 = [String](menuItem.item.getItem().Extras!.keys)
         let arrayOfFuckSwift = [Double](menuItem.item.getItem().Extras!.values)
        let extra = arrayOfFuckSwift2[indexPath!.row]
        let price = arrayOfFuckSwift[indexPath!.row]
        if (sender.on) {
            delegate?.giveAdditionalPrice(price)
            delegate?.giveAdditionalExtras(extra)
        }
        else {
            delegate?.removeAdditionalExtras(extra)
            delegate?.removeAdditionalPrice(-price)
            
        }
    }
    

}
