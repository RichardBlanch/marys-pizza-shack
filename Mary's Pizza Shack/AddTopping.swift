//
//  AddTopping.swift
//  Mary's Pizza Shack
//
//  Created by Rich Blanchard on 8/16/15.
//  Copyright (c) 2015 Rich. All rights reserved.
//

import Foundation
protocol addTopping {
    func addTopping(pizzaTopping:PizzaWithTopping)
    func removeTopping(pizzaTopping:PizzaWithTopping)
}
