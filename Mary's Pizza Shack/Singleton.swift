//
//  Singleton.swift
//  Mary's Pizza Shack
//
//  Created by Rich Blanchard on 8/10/15.
//  Copyright (c) 2015 Rich. All rights reserved.
//

import Foundation
class CurrencyFormatter:NSNumberFormatter {
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    override init() {
        super.init()
        self.locale = NSLocale.currentLocale()
        self.maximumFractionDigits = 2
        self.minimumFractionDigits = 2
        self.alwaysShowsDecimalSeparator = true
        self.numberStyle = .CurrencyStyle
    }
    class var sharedInstance: CurrencyFormatter {
        //curly braces say read only computed propertu. computers every time called
        //instance of currensyformatter everytime called
        struct Static {
            static let instance = CurrencyFormatter()
            
        }
        return Static.instance
    }
    
}


