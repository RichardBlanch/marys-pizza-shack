//
//  CustomFlowLayout.swift
//  Mary's Pizza Shack
//
//  Created by Rich Blanchard on 8/10/15.
//  Copyright (c) 2015 Rich. All rights reserved.
//

import UIKit



class CustomFlowLayout: UICollectionViewFlowLayout {
    let zoomF = CGFloat(0.25)
   
    override func prepareLayout() {
    scrollDirection = .Horizontal
    let size = self.collectionView?.frame.size
    
    let itemWidth = size!.width / 3.5
    self.itemSize = CGSizeMake(itemWidth, itemWidth*0.75)
    self.sectionInset = UIEdgeInsetsMake(0, 0, 0, 0)
        
    }
    override func shouldInvalidateLayoutForBoundsChange(newBounds: CGRect) -> Bool {
    return true
    }
    override func layoutAttributesForElementsInRect(rect: CGRect) -> [AnyObject]? {
        
        var attributes = super.layoutAttributesForElementsInRect(rect)
       
        if let collectionViewA = self.collectionView
        {
        var visibleRect = CGRectZero
        visibleRect.origin = collectionViewA.contentOffset
        visibleRect.size = collectionViewA.frame.size
        
        var collectionViewHalfFrame = collectionViewA.frame.size.width / 2
            if let att = attributes
            {
            for attribute   in att
            {
                if let layoutAttribute = attribute as? UICollectionViewLayoutAttributes
                {
                
                if(CGRectIntersectsRect(attribute.frame, rect))
                {
                    var distance = CGRectGetMidX(visibleRect) - attribute.center.x
                    var normalizedDistance = distance / collectionViewHalfFrame
                    
                    
                    
                    if(abs(distance) < collectionViewHalfFrame)
                    {
                    var zoom = (1 - abs(normalizedDistance))
                    zoom *= zoomF
                    zoom += 1
                    var rotationTransform = CATransform3DMakeRotation(normalizedDistance * CGFloat(M_PI_2 * 0.8), 0.0, 1.0, 0.0)
                    var zoomTransform = CATransform3DMakeScale(zoom, zoom, 1.0)
                    layoutAttribute.transform3D = CATransform3DConcat(zoomTransform, rotationTransform)
                    layoutAttribute.zIndex = Int(abs(normalizedDistance))
                    var alpha = (1-abs(normalizedDistance))
                    if(alpha > 1.0)
                    {alpha = 1.0; layoutAttribute.alpha = alpha}
                    
                        
                    

                    
                    
                    }
                    else {
                        layoutAttribute.alpha = 0.0
                    }
                    
                }
                }
                
            }
                
          
            }
            
        
        
        }
        
        return attributes
    }
   
}
