//
//  BackTableVC.swift
//  Mary's Pizza Shack
//
//  Created by Rich Blanchard on 8/11/15.
//  Copyright (c) 2015 Rich. All rights reserved.
//

import Foundation
import UIKit
class BackTableVC: UITableViewController {
    var TableArray = [String]()
   // var zoomingTransition : ZoomingTransition!
    
    override func viewDidLoad() {
        self.view.backgroundColor = UIColor.whiteColor()
        self.view.contentMode = UIViewContentMode.ScaleAspectFit
        
        
        
        
        TableArray = ["Menu", "Cart", "Locations", "Create Pizza"]
        
    }
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return TableArray.count
    }
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("Hello", forIndexPath: indexPath) as! SliderMenuCustomTableViewCell
        cell.backgroundColor = UIColor.whiteColor()
        
        cell.labelForMenu?.text = TableArray[indexPath.row]
        return cell
    }
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        performSegueWithIdentifier(TableArray[indexPath.row], sender: self)
    }
 
   
}