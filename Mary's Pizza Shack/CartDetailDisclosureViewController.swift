//
//  CartDetailDisclosureViewController.swift
//  Mary's Pizza Shack
//
//  Created by Rich Blanchard on 8/12/15.
//  Copyright (c) 2015 Rich. All rights reserved.
//

import UIKit

class CartDetailDisclosureViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    var cartItem: CartItem!
   
    @IBOutlet weak var menuItemLabel: UILabel!
    var itemDescriptionArray : [String]!
   
    @IBOutlet weak var tableView: UITableView!
    
  
   

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        itemDescriptionArray = []
        menuItemLabel.text = cartItem.item
        menuItemLabel.backgroundColor = UIColor.tanBackground()
        let mutablePrice = "Price: " + cartItem.price
        itemDescriptionArray.append(mutablePrice)
        
     
        if let extra = cartItem.extras {
            if extra.count > 0 {
                var mutableString:String = ""
                for string in extra {
                    mutableString += " \(string)"
                }
            mutableString = "Extras: " + mutableString
                itemDescriptionArray.append(mutableString)
            
            }
        }
        if let size = cartItem.size {
            itemDescriptionArray.append(size)
        }
        if let sauce = cartItem.sauces {
            itemDescriptionArray.append(sauce)
        }
        if cartItem.item == "Create-Your-Own-Pizza" {
           itemDescriptionArray = []
            itemDescriptionArray.append(cartItem.item)
            itemDescriptionArray.append(cartItem.size!)
            if let extras = cartItem.extras
            {
            for extra in extras {
                itemDescriptionArray.append(extra)
            }
            }
            
        }
      
        
        
    }
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return itemDescriptionArray.count
    }
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("cellDetail", forIndexPath: indexPath) 
        cell.textLabel?.textColor = UIColor.marysGreen()
        cell.textLabel?.font = UIFont.maryBoldSmall()
        cell.textLabel!.text = itemDescriptionArray[indexPath.row]
        
        return cell
        
    }
 
    @IBAction func dismissVC(sender: AnyObject) {
    dismissViewControllerAnimated(true, completion: nil)
    }
    override func prefersStatusBarHidden() -> Bool {
        return true
    }

    
}
