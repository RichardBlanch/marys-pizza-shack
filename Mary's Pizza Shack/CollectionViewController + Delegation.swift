//
//  CollectionViewController + Delegation.swift
//  Mary's Pizza Shack
//
//  Created by Rich Blanchard on 8/14/15.
//  Copyright (c) 2015 Rich. All rights reserved.
//

import Foundation

extension InstagamPhotosViewController {
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if(imageCacher.shardCacher.urls.count > 12) {
            activityView.stopAnimating()
            activityView.hidden = true
        }
        return imageCacher.shardCacher.urls.count
    }
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("Cell", forIndexPath: indexPath) as! PhotosCollectionViewCell
        var url: String?
        
        
        
        let index = imageCacher.shardCacher.urls[imageCacher.shardCacher.urls.startIndex.advancedBy(indexPath.row)]
        
        if let data = imageCacher.shardCacher.cache.objectForKey(index) as? NSData
        {
           
            cell.instagramPhoto.image = UIImage(data: data)
        }
            
            
            
        else {
            fetchPicture()
            // cell.instagramPhoto.image = UIImage(named: "mary")
        }
        
        
        
        return cell
    }
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAtIndex section: Int) -> UIEdgeInsets {
        let insets = UIEdgeInsetsMake(0, 0, 5, 0)
        
        
        return insets
    }
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAtIndex section: Int) -> CGFloat {
        let float = CGFloat(2.0)
        return float
    }
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        let size = CGSizeMake(screenRect.width / 3 - 5, 100)
        return size
        
    }
    override func  prefersStatusBarHidden() -> Bool {
        return true
    }
}
