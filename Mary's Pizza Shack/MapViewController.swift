//
//  MapViewController.swift
//  Mary's Pizza Shack
//
//  Created by Rich Blanchard on 8/13/15.
//  Copyright (c) 2015 Rich. All rights reserved.
//

import UIKit
import MapKit

class MapViewController: UIViewController, MKMapViewDelegate {
    
    @IBOutlet weak var mapView: MKMapView!
    var arrayOfRestuarants: [Restaurant]!
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(true)
        self.tabBarController?.tabBar.hidden = true
    }
    
    override func viewDidLoad() {
        mapView.delegate = self
        mapView.showsUserLocation = true
        let shownRegion = MKCoordinateRegion(center: locationHandler.sharedManager.location.coordinate, span: MKCoordinateSpan(latitudeDelta: 3, longitudeDelta: 2))
        mapView.mapType = MKMapType.Hybrid
        mapView.region = shownRegion
        arrayOfRestuarants = Restaurants.getArray()
        for rest in arrayOfRestuarants
        {
            if  rest.coordinates.coordinate.latitude > 0 && rest.name != nil && rest.address != nil
            {
            rest.coordinate = rest.coordinates.coordinate
            rest.title = rest.name
            rest.subtitle = rest.address
            
            mapView.addAnnotation(rest)
            }
            
        }
        
    }
    func mapView(mapView: MKMapView, viewForAnnotation annotation: MKAnnotation) -> MKAnnotationView? {
        
        
        if (annotation is MKUserLocation) {
            return nil
        }
        
        let customPinView = MKPinAnnotationView(annotation: annotation, reuseIdentifier: "String")
        customPinView.animatesDrop = true
        customPinView.canShowCallout = true
        customPinView.frame.size = CGSizeMake(400, 400)
        var something = customPinView.annotation
        customPinView.image = UIImage(named: "rsz_1small_icon")
        
        
        let restaurant = annotation as! Restaurant
        
      let rightButton = UIButton(type: UIButtonType.DetailDisclosure)
     customPinView.rightCalloutAccessoryView = rightButton as UIButton
        rightButton.addTarget(self, action: "goToMaps:", forControlEvents: .TouchUpInside)
       
        
        // Add a custom image to the left side of the callout.
        
        var imageForRestaurant:UIImage? = UIImage(named: restaurant.name)
        if let imageForEatery = imageForRestaurant {
        let imageView = UIImageView()
        imageView.image = imageForEatery
        imageView.frame = CGRectMake(5, 5, 55, 55)
        customPinView.leftCalloutAccessoryView = imageView;
        }
        else {
            print(restaurant.name)
        }
        
        return customPinView;
    }
    func mapView(mapView: MKMapView, didSelectAnnotationView view: MKAnnotationView) {
        print("I am being called", terminator: "")
        
    }
    func mapView(mapView: MKMapView, annotationView view: MKAnnotationView, calloutAccessoryControlTapped control: UIControl) {
        
        print("I am being called", terminator: "")
        let controller = UIAlertController(title: "WARNING:", message: "This will take you to the Maps app.\nWould you like directions?", preferredStyle: UIAlertControllerStyle.Alert)
        let ok = UIAlertAction(title: "Yes", style: UIAlertActionStyle.Default) { (alert) -> Void in
            
            let placemark = MKPlacemark(coordinate: view.annotation!.coordinate, addressDictionary: nil)
            
            let mapItem = MKMapItem(placemark: placemark)
            
            MKMapItem.openMapsWithItems([mapItem, MKMapItem.mapItemForCurrentLocation()], launchOptions: [MKLaunchOptionsDirectionsModeKey : MKLaunchOptionsDirectionsModeDriving])
            
        }
        let cancel = UIAlertAction(title: "No", style: UIAlertActionStyle.Cancel) { (Action) -> Void in
        }
        controller.addAction(ok)
        controller.addAction(cancel)
        UIApplication.sharedApplication().keyWindow?.rootViewController?.presentViewController(controller, animated: true, completion: nil)
        
        
    }
    
    @IBAction func dismiss(sender: AnyObject) {
        dismissViewControllerAnimated(true, completion: nil)
    }
    func goToMaps(sender:UIButton) {
        
        print("I am here", terminator: "")
        print("\(sender.superview)", terminator: "")

        //        let controller = UIAlertController(title: "WARNING:", message: "This will take you to the Maps app.\nWould you like directions?", preferredStyle: UIAlertControllerStyle.Alert)
//        let ok = UIAlertAction(title: "Yes", style: UIAlertActionStyle.Default) { (alert) -> Void in
//            
//            let placemark = MKPlacemark(coordinate: view.annotation.coordinate, addressDictionary: nil)
//            
//            let mapItem = MKMapItem(placemark: placemark)
//            
//            MKMapItem.openMapsWithItems([mapItem, MKMapItem.mapItemForCurrentLocation()], launchOptions: [MKLaunchOptionsDirectionsModeKey : MKLaunchOptionsDirectionsModeDriving])
//            
//        }
//        let cancel = UIAlertAction(title: "No", style: UIAlertActionStyle.Cancel) { (Action) -> Void in
//        }
//        controller.addAction(ok)
//        controller.addAction(cancel)
//        UIApplication.sharedApplication().keyWindow?.rootViewController?.presentViewController(controller, animated: true, completion: nil)
//        
    }
    
    
}







