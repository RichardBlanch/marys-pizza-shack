//
//  Desserts.swift
//  Mary's Pizza Shack
//
//  Created by Rich Blanchard on 8/8/15.
//  Copyright (c) 2015 Rich. All rights reserved.
//

import Foundation
import UIKit

class Deserts: MenuItem {
    class func getItemsAndImage() -> (array:[Deserts] , image: UIImage ) {
        
        let items = [Deserts(item: .BlueberryCrumbleCheesecake) , Deserts(item: .MarysOriginalMudPie), Deserts(item: .SaltedCaramelGelatoPie), Deserts(item: .GiganticCookieSundae), Deserts(item: .MarysMessyBrownieSundae), Deserts(item: .PineappleUpsideDownCake)]
        
        let image = UIImage(named: "desserts_button")
        
        return (array: items, image:image!)
    }
}



 