//
//  LocationsTableViewCell.swift
//  Mary's Pizza Shack
//
//  Created by Rich Blanchard on 8/13/15.
//  Copyright (c) 2015 Rich. All rights reserved.
//

import UIKit

class LocationsTableViewCell: UITableViewCell {
    
    
    var location : CLLocation!
    
    
    var resturant: Restaurant? {
        didSet {
           
            
           
           
            self.distanceLabel.text = (String(format: "%.2f Miles", resturant!.distance))
            
            let attributedText = NSAttributedString(string: resturant!.name, attributes: [NSUnderlineStyleAttributeName : 1.0, ])
            restaurantLabel.attributedText = attributedText
            
            addressLabel.text = resturant?.address
            
            phoneNumberTextView.text = resturant?.phoneNumber
            
            hoursLabe.text = resturant?.hours
            
            restaurantImageView.image = UIImage(named: resturant!.name!)
            
        
            
        }
    
    }

    @IBOutlet weak var restaurantLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var phoneNumberTextView: UITextView!
    @IBOutlet weak var hoursLabe: UILabel!
    @IBOutlet weak var distanceLabel: UILabel!
    
    @IBOutlet weak var restaurantImageView: UIImageView!
    
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }


    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        selectionStyle = UITableViewCellSelectionStyle.None
    }

}
