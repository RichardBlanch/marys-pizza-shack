//
//  DetailMenuController.swift
//  Mary's Pizza Shack
//
//  Created by Rich Blanchard on 8/7/15.
//  Copyright (c) 2015 Rich. All rights reserved.
//

import UIKit
import QuartzCore



  class DetailMenuController: UIViewController, UITableViewDataSource, UITableViewDelegate,  addSauces, takeToSauces, giveState {
    var menuItem: MenuItem!
    
    var selectedSegment: String?
    
    var selectedSegmentString: String?
    
    var arrayForExtras: [String]?
    
    var pricesForExtras: [Double]!
    
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var itemLabel: UILabel!
    
    @IBOutlet weak var segmentedControl: UISegmentedControl!
    
    @IBOutlet weak var extrasLabel: UILabel!
    
    @IBOutlet weak var priceLabel: UILabel!
    
    @IBOutlet weak var pickerViewButton: UIButton!
    
    @IBOutlet weak var orderItemButton: UIButton!
    
    @IBOutlet weak var descriptionTextView: UITextView!
    
    var sauceToAdd : String!
    
    var arrayOfPrices: [String]!
    
    var arrayOfNewItem: [String]!
    
    var timer : NSTimer!
    var customAlertView : CustomAlertView!
    var additionalPrice : Double!
    
    override  func viewWillAppear(animated: Bool) {
       
        tableView.backgroundColor = UIColor.tanBackground()
        navigationController?.navigationBar.hidden = true
        tabBarController?.tabBar.hidden = true
        if let thereIsSegmentNeeded = menuItem.item.getItem().prices
        
        {
            
            arrayOfNewItem = MenuItem.sortDictionary(thereIsSegmentNeeded).Sizes //get all possible sizes
            segmentedControl.removeAllSegments()
            for var i = 0; i < arrayOfNewItem.count; i++ {
                segmentedControl.insertSegmentWithTitle(arrayOfNewItem[i], atIndex: i, animated: false)
            }
            let string = MenuItem.sortDictionary(thereIsSegmentNeeded).Prices[0] as NSString
            let double = string.doubleValue
            priceLabel.text = CurrencyFormatter.sharedInstance.stringFromNumber(NSNumber(double: double))
            segmentedControl.selectedSegmentIndex = 0
            selectedSegmentString = MenuItem.sortDictionary(thereIsSegmentNeeded).Sizes[0]
        }
        else {
            segmentedControl.hidden = true
        }
    }
    
    override  func viewDidLoad() {
        super.viewDidLoad()
         additionalPrice = 0.0
        
        
        arrayForExtras = []
        pricesForExtras = []
        setUpAnimations() //Extras label
        
        
        
        if let isThereExtras =  menuItem.item.getItem().Extras
        {
         arrayForExtras = []
        }
        else {
            self.tableView.hidden = true
            replaceTableViewWithImageView()
        }
        
        
        
        if let item = menuItem {
            
            itemLabel.text = item.item.getItem().itemName
            descriptionTextView.text = item.item.getItem().itemDescription
            priceLabel.text = CurrencyFormatter.sharedInstance.stringFromNumber(NSNumber(double: item.item.getItem().defaultPrice))
            if let pickerView = item.item.getItem().sauces {
                
            }
            else {
                pickerViewButton.hidden = true
                pickerViewButton = nil
            }
            
        }
    }
    
    
    
    
    @IBAction func changePrice(sender: UISegmentedControl) {
        
      
       
        
        
        if let prices =  menuItem.item.getItem().prices
        {
            print("The prices are\(pricesForExtras)")
            print("The EXTRAS are\(arrayForExtras)")
            additionalPrice = 0.0
           
            if pricesForExtras.count > 0 {
                for double in pricesForExtras
                {
                    additionalPrice! += double
                }
            
            }
            else {
                additionalPrice! = 0.0
            }
            
            arrayOfPrices = MenuItem.sortDictionary(prices).Prices //sort prices cheapest price first
            var arrayOfString = MenuItem.sortDictionary(prices).Sizes
            let index = sender.selectedSegmentIndex
            selectedSegment = arrayOfPrices[index]
            selectedSegmentString = arrayOfString[index]
            let string = self.arrayOfPrices[index] as NSString
            var changedPrice = string.doubleValue
            changedPrice += additionalPrice
            let changedPriceInCorrectForm = CurrencyFormatter.sharedInstance.stringFromNumber(NSNumber(double: changedPrice))
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                self.priceLabel.text = changedPriceInCorrectForm
            })
        }
        
    }
    
    @IBAction func dismiss(sender: AnyObject) {
        navigationController?.popViewControllerAnimated(true) //pop viewController
        tabBarController?.tabBar.hidden = false
    }
    //go to blur view
    @IBAction func change(sender: UIButton) {
        let fmVC = PickSaucesViewController(fromView: sender)
        fmVC.item = menuItem
        fmVC.delegate = self
        if let _ = customAlertView {
            
            customAlertView?.removeFromSuperview()
        }
        self.presentViewController(fmVC, animated: true, completion: nil)
    }
    
    @IBAction func order(sender: UIButton) {
        
        
        if(arrayForExtras != nil && arrayForExtras!.count == 0) {
            arrayForExtras = []
        }
        
        ///THIS IS FOR SAUCES
        var itemToAdd:CartItem;
        
        if let _ = menuItem.item.getItem().sauces
        {
            if sauceToAdd == nil {
                createSaucePickerView()
            }
                
            else {
                 itemToAdd =  CartItem(price: self.priceLabel.text!, item: self.itemLabel.text!, extras: arrayForExtras, sauces: self.sauceToAdd , size: selectedSegmentString)
               
                showBanner(itemToAdd)
                Cart.sharedCart.addItem(itemToAdd)
                callTimer()
                
            }
        }
            
            
        else {
              itemToAdd =  CartItem(price: self.priceLabel.text!, item: self.itemLabel.text!, extras: arrayForExtras, sauces: self.sauceToAdd , size: selectedSegmentString)
            showBanner(itemToAdd)
            Cart.sharedCart.addItem(itemToAdd)
            callTimer()
        }
        
    }
    
}




