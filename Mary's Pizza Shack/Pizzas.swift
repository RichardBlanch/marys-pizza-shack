//
//  Pizzas.swift
//  Mary's Pizza Shack
//
//  Created by Rich Blanchard on 8/8/15.
//  Copyright (c) 2015 Rich. All rights reserved.
//

import Foundation
import UIKit

class Pizza : MenuItem
{
    
    
    class func getItemsAndImages() -> (array: [Pizza], image:UIImage)
    {
        let items = [Pizza(item: .MarysCombinations), Pizza(item: .TotosCombo), Pizza(item: .HawaiianLuau), Pizza(item: .BuffaloChicken), Pizza(item: .TuscanGarlicChicken), Pizza(item: .PestoChicken), Pizza(item: .BBQChicken), Pizza(item: .PizzaAlaSriracha), Pizza(item: .ClassicCheeseCalzone), Pizza(item: .VegetarianCalzone), Pizza(item: .MarysMeatCalzone)]
        
        let image = UIImage(named: "pizzas-button")
        
        
       
        
        return (array: items, image: image!)
    }

}