//
//  PastasAndEntrees.swift
//  Mary's Pizza Shack
//
//  Created by Rich Blanchard on 8/8/15.
//  Copyright (c) 2015 Rich. All rights reserved.
//

import Foundation
import UIKit

class PastasAndEntrees : MenuItem {
    class func getItemsAndImages() ->(array:[PastasAndEntrees], image:UIImage)
    {
        let items = [PastasAndEntrees(item: .SpaghettiWithAGiantMeatball), PastasAndEntrees(item: .GnoochiWithAGiantMeatball), PastasAndEntrees(item: .HeAndShe), PastasAndEntrees(item: .LinguineMarinara), PastasAndEntrees(item: .PennewithSausage), PastasAndEntrees(item: .ProteinPlate) , PastasAndEntrees(item: .ChickenFettuccineAlfredo) , PastasAndEntrees(item: .ChickenPicatta) , PastasAndEntrees(item: .MeatRaviolli) , PastasAndEntrees(item: .CheeseRaviolli) , PastasAndEntrees(item: .TortelliniallaMarys) , PastasAndEntrees(item: .MarysClassicLasagna), PastasAndEntrees(item: .ChickenParmigiana) , PastasAndEntrees(item: .EggplantParmigiana)]
        
        let image = UIImage(named: "Pastas_And_Entrees_Button")
        
        return (array:items, image: image!)
    }
}
