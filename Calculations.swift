//
//  Calculations.swift
//  Mary's Pizza Shack
//
//  Created by Rich Blanchard on 8/11/15.
//  Copyright (c) 2015 Rich. All rights reserved.
//

import Foundation
import UIKit
import MapKit


class Calculator {
    let defaults = NSUserDefaults.standardUserDefaults()
    class func getCorrectString(item:CartItem)->String {
        
        var mutableString:String = "\(item.item)\n\n"
        if let changedSize = item.size {
            mutableString += "\(changedSize)\n\n"
        }
        if let extras = item.extras {
           
            var extrasString = ""
            if(extras.count > 0)
            {
                
                extrasString = extras[0]
                for var i = 1; i < extras.count; i++ {
                    extrasString += ", \(extras[i])"
                }
                
                mutableString += "\(extrasString)\n\n"
               
            }
           
        }
        
        if let sauces = item.sauces {
            mutableString += "\(sauces)\n\n"
        }
        
        return mutableString
    }
    class func getCurrentTextPrice(label: UILabel , doublePassed:Double)->Double
    {
        let newString = label.text!.stringByReplacingOccurrencesOfString("$", withString: "", options: [], range: nil)
        let currentPrice  = newString as NSString
        var doubleValue = currentPrice.doubleValue
        doubleValue += doublePassed
        
        return doubleValue
    }
    class func getOrder(itemsInCart:[CartItem])->String {
        var mutableString = ""
        for item in itemsInCart {
          
            mutableString += "Order: "
            mutableString += item.item
            mutableString += " "
            
            if let size = item.size
           {
           mutableString += "Size: "
            mutableString += size
            mutableString += " "
            }
          
            
            
            if let sauces = item.sauces {
           mutableString += "Sauce: "
            mutableString += sauces
            mutableString += " "
            }
            if let extras = item.extras {
                for extra in extras {
                    mutableString += extra
                    mutableString += " "
                }
            }
            mutableString += "\n\n"
            
           
        }
        return mutableString
    }
     
    
    
//    class func setUpAlertController(annotationView:MKAnnotationView, mapView: MKMapView) {
//        let alertControlelr = UIAlertController(title: "Directions", message: "Would you like Directions to \(annotationView.annotation!.title)", preferredStyle: UIAlertControllerStyle.Alert)
//        let ok = UIAlertAction(title: "Yes", style: UIAlertActionStyle.Default) { (action) -> Void in
//            
//            let request = MKDirectionsRequest()
//            request.setSource = MKMapItem.mapItemForCurrentLocation()
//            
//            
//            request.setDestination = MKMapItem.mapItemForCurrentLocation()
//            let directions = MKDirections(request: request)
//            directions.calculateDirectionsWithCompletionHandler({ (response, error) -> Void in
//                if error != nil {
//                    print("Error is \(error.localizedDescription)", terminator: "")
//                }
//                else {
//                    var route = response!.routes.last as! MKRoute
//                    //routeMap.addOverlay(route.polyline,
//                    //  level: MKOverlayLevel.AboveRoads)
//                    mapView.addOverlay(route.polyline, level:MKOverlayLevel.AboveRoads)
//                    
//                    
//                }
//            })
    
            
//            
//        }
//        alertControlelr.addAction(ok)
//        UIApplication.sharedApplication().keyWindow?.rootViewController?.presentViewController(alertControlelr, animated: true, completion: nil)
//    }
}


