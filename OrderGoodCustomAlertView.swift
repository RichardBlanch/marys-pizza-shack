//
//  OrderGoodCustomAlertView.swift
//  Mary's Pizza Shack
//
//  Created by Rich Blanchard on 8/11/15.
//  Copyright (c) 2015 Rich. All rights reserved.
//

import UIKit
import QuartzCore
@objc protocol cartAlert
{
    func goToCart()
    optional func enable()
   optional func disable()
}

class OrderGoodCustomAlertView: UIView {
    var textForTextView: String!
        {
        didSet {
            createView()
        }
    }
    var delegate: cartAlert?
   
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
       
    }
    
    func createView() {
        self.frame = CGRect(x: (screenRect.width / 2) - 150 , y: (screenRect.height / 2) - 150 , width: 300, height: 300)
        backgroundColor = UIColor.yellowGifBackground()
        let animation = CABasicAnimation(keyPath: "position.y")
        animation.fromValue = 800.0
        animation.duration = 1.0
        animation.toValue = (screenRect.height) / 2 - 150
        self.layer.addAnimation(animation, forKey: nil)
        createButtons()
        createTextView()
        delegate?.disable!()
        
        
    }
    
    func createButtons() {
        var cancel = UIButton(frame: CGRectMake(self.frame.width - 40, (self.frame.origin.y / 8) , 30, 30))
        cancel.setTitle("X", forState: .Normal)
        cancel.addTarget(self, action: "cancel", forControlEvents: .TouchUpInside)
        self.addSubview(cancel)
        
         var okay = UIButton(frame: CGRectMake(self.frame.width / 2 - 100, self.frame.height - 75, 200, 50))
        if let font = UIFont(name: "Cabin-Bold", size: 20)
        {
            let colour = UIColor.marysRed()
            let attributes: [NSString : AnyObject] = [NSFontAttributeName: font, NSForegroundColorAttributeName: colour]
            
            
            var attributedString = NSAttributedString(string: "Go To Cart", attributes: [NSFontAttributeName: font, NSForegroundColorAttributeName: colour])
            okay.setAttributedTitle(attributedString, forState: .Normal)
            okay.setTitleColor(UIColor.marysGreen(), forState: .Normal)
            okay.backgroundColor = UIColor.whiteColor()
            okay.layer.cornerRadius = 5.0
            okay.addTarget(self, action: "goToCart", forControlEvents: .TouchUpInside)
            addSubview(okay)
        }
    }
    func createTextView() {
        
        let label = UILabel(frame: CGRectMake(self.frame.origin.x,  (self.frame.origin.y / 8), 250, 30))
         let attributedText = NSAttributedString(string: "ITEM ADDED TO CART:", attributes: [NSForegroundColorAttributeName : UIColor.marysGreen(), NSUnderlineStyleAttributeName : 1.0, NSFontAttributeName : UIFont.maryBold()])
        label.font = UIFont.maryBold()
        label.attributedText = attributedText
        addSubview(label)
        
        
        
        
        let textView = UITextView(frame: CGRectMake(self.frame.origin.x, label.frame.origin.y + 22, self.frame.width - 15, 150))
        textView.editable = false
        textView.selectable = false
        textView.scrollEnabled = false
        textView.font = UIFont(name: "Cabin-Bold", size: 16)
        textView.backgroundColor = UIColor.clearColor()
        textView.textColor = UIColor.marysRed()
        textView.text = textForTextView
        addSubview(textView)
        
        
       
    }
    func cancel()
    {
        removeFromSuperview()
        delegate?.enable!()
        
    }
    func goToCart() {
        delegate?.goToCart()
    }


}
